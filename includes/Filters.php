<?php

namespace NM_Pdf;

use \NmeriMedia\V1\Bootstrap\Popover;
use NM_Pdf\Models\MetaboxButton;

defined( 'ABSPATH' ) || exit;

class Filters {

	public static function run() {
		add_action( 'add_meta_boxes', [ __CLASS__, 'add_meta_boxes' ] );
		add_action( 'woocommerce_admin_order_actions_end', [ __CLASS__, 'add_admin_order_action' ] );
	}

	public static function add_meta_boxes() {
		foreach ( array_keys( nm_pdf()->get_pdf_objects() ) as $objec_type ) {
			add_meta_box( 'nmpdf-metabox', __( 'NM PDF', 'nm-pdf' ), [ __CLASS__, 'metabox' ], $objec_type, 'side', 'high', [ 'object_type' => $objec_type ] );
		}
	}

	public static function metabox( $post, $args ) {
		$object_type = $args[ 'args' ][ 'object_type' ];
		$object_class = nm_pdf()->get_pdf_objects()[ $object_type ] ?? null;

		if ( $object_class ) {
			$object = new $object_class( [ 'object_id' => $post->ID ?? 0 ] );

			do_action( 'nm_pdf_object_metabox_before', $object, $args );

			echo '<ul class="nm-pdf object-actions">';

			foreach ( array_keys( $object->get_pdf_types() ) as $pdf_type ) {
				$metabox_btn = new MetaboxButton( $object, $pdf_type );
				echo '<li>' . $metabox_btn->get() . '</li>';
			}

			echo '</ul>';

			do_action( 'nm_pdf_object_metabox_after', $object, $args );
		}
	}

	public static function add_admin_order_action( $order ) {
		if ( 'trash' === $order->get_status() ) {
			return;
		}

		$object_type = 'shop_order';
		$object_id = $order->get_id();
		$object_class = nm_pdf()->get_pdf_objects()[ $object_type ] ?? null;

		if ( $object_class ) {
			$object = new $object_class( [ 'object_id' => $object_id ?? 0 ] );

			if ( $object->get_pdf_types() ) {
				$button = new Button();
				$button->icon_args[ 'size' ] = '16px';
				$button->icon_args[ 'style' ] = 'position:absolute;left:50%;top:50%;transform:translate(-50%,-50%)';
				$icon = $button->get_icon();

				$popover = new Popover();
				$popover->set_text( $icon );
				$popover->set_title( __( 'Create PDF', 'nm-pdf' ) );
				$popover->dismiss_on_click();
				$popover->set_direction( 'left' );
				$popover->add_container_class( 'wc-action-button' );
				$popover->container_attributes[ 'style' ] = 'display:inline-flex;align-items:center;justify-content:center;';

				$content = '<ul>';
				foreach ( $object->get_pdf_types() as $type => $text ) {
					$btn = new Button( [
						'object_id' => $object_id ?? '',
						'object_type' => $object_type,
						'pdf_type' => $type,
						] );

					$content .= '<li><a href="' . esc_url( $btn->get_url() ) . '">' . esc_html( $text ) . '</a></li>';
				}
				$content .= '</ul>';

				$popover->set_content( $content );
				echo $popover->get();
			}
		}
	}

}
