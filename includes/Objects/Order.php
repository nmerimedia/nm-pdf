<?php

namespace NM_Pdf\Objects;

defined( 'ABSPATH' ) || exit;

class Order extends BaseObject {

	public $order_id;
	public $order;

	public function __construct( $data = [] ) {
		parent::__construct( $data );
		$this->order_id = $this->object_id;
	}

	public function is_valid() {
		return is_a( $this->get_order(), '\WC_Order' );
	}

	public function get_object_type() {
		return 'shop_order';
	}

	public function get_pdf_types() {
		return apply_filters( 'nm_pdf_object_pdf_types',
			[
				'invoice' => [
					'title' => __( 'Invoice', 'nm-pdf' ),
				],
				'packing-slip' => [
					'title' => __( 'Packing Slip', 'nm-pdf' ),
				],
			],
			$this
		);
	}

	public function get_order() {
		$this->order = $this->order ? $this->order : wc_get_order( $this->order_id );
		return $this->order;
	}

	public function get_billing_address() {
		return $this->get_order()->get_formatted_billing_address();
	}

	public function get_shipping_address() {
		return $this->get_order()->get_formatted_shipping_address();
	}

	public function get_order_data() {
		return [
			'order_number' => [
				'title' => __( 'Order Number', 'nm-pdf' ),
				'value' => $this->get_order()->get_order_number()
			],
			'order_date' => [
				'title' => __( 'Order Date', 'nm-pdf' ),
				'value' => wc_format_datetime( $this->get_order()->get_date_created() )
			],
			'invoice_date' => [
				'title' => __( 'Invoice Date', 'nm-pdf' ),
				'value' => wp_date( get_option( 'date_format' ) )
			],
			'shipping_method' => [
				'title' => __( 'Shipping Method', 'nm-pdf' ),
				'value' => $this->get_order()->get_shipping_method()
			],
		];
	}

	/**
	 * Get the line items in the order
	 * @return array
	 */
	public function get_order_items() {
		return $this->get_order()->get_items();
	}

	public function get_order_item_totals() {
		return $this->get_order()->get_order_item_totals();
	}

	/**
	 * Get the properties for order line items that should be shown in the template
	 *
	 * If the property is prefixed with 'product_' it would get the property from the
	 * WC_Product class. Else it would get it from the WC_Order_Item_Product class.
	 * For example to get the product weight, add the element to the array:
	 * 'product_weight' => __('Weight', 'nm-pdf').
	 * This would get the weight directly from the WC_Product class,
	 *
	 * @return array
	 */
	public function get_order_items_data() {
		return [
			'product_image' => __( 'Image', 'nm-pdf' ),
			'name' => __( 'Product', 'nm-pdf' ),
			'quantity' => __( 'Quantity', 'nm-pdf' ),
			'subtotal' => __( 'Price', 'nm-pdf' ),
		];
	}

	public function get_customer_note() {
		return $this->get_order()->get_customer_note();
	}

	public function get_order_items_template() {
		return nm_pdf()->get_wc_template( $this->get_object_type() . '/order-items', [ 'object' => $this ] );
	}

	/**
	 * Get the item meta that should be hidden from display
	 * (Always check to make sure this list is accurate on updates)
	 *
	 * @return array Default woocommerce hidden order item meta.
	 */
	public function get_hidden_order_itemmeta() {
		return apply_filters( 'woocommerce_hidden_order_itemmeta',
			array(
				'_qty',
				'_tax_class',
				'_product_id',
				'_variation_id',
				'_line_subtotal',
				'_line_subtotal_tax',
				'_line_total',
				'_line_tax',
				'method_id',
				'cost',
				'_reduced_stock',
				'_restock_refunded_items',
			)
		);
	}

	public function get_item_meta( $item ) {
		$meta = [];
		$model = $this->get_item_model( $item );
		$hidden_itemmeta = $this->get_value( 'hidden_order_itemmeta' );

		if ( !in_array( '_sku', $hidden_itemmeta ) ) {
			$sku = $model->get_value( 'product_sku' );
			if ( $sku ) {
				$meta[ __( 'SKU', 'nm-pdf' ) ] = $sku;
			}
		}

		if ( !in_array( '_variation_id', $hidden_itemmeta ) ) {
			$variation_id = $model->get_value( 'product_variation_id' );
			if ( $variation_id && 'product_variation' === get_post_type( $variation_id ) ) {
				$meta[ __( 'Variation ID', 'nm-pdf' ) ] = $variation_id;
			}
		}

		foreach ( $item->get_formatted_meta_data( '' ) as $data ) {
			if ( !in_array( $data->key, $hidden_itemmeta, true ) ) {
				$meta[ $data->display_key ] = $data->display_value;
			}
		}
		return $meta;
	}

	/**
	 * Get the utility model for querying item properties
	 * @param \WC_Order_Item_Product $line_item
	 * @return \NM_Pdf\Models\OrderItemProduct
	 */
	public function get_item_model( $line_item ) {
		return new \NM_Pdf\Models\OrderItemProduct( $line_item );
	}

	public function filter_value( $value, $key, ...$args ) {
		if ( 'packing-slip' === $this->get_pdf_type() ) {
			if ( in_array( $key, [ 'billing_address', 'order_item_totals' ] ) ) {
				$value = null;
			}

			if ( 'order_items_data' === $key ) {
				unset( $value[ 'subtotal' ] );
			}

			if ( 'order_data' === $key ) {
				unset( $value[ 'invoice_date' ] );
			}
		} elseif ( 'invoice' === $this->get_pdf_type() ) {
			if ( in_array( $key, [ 'shipping_address' ] ) ) {
				$value = null;
			}

			if ( 'order_data' === $key ) {
				unset( $value[ 'shipping_method' ] );
			}
		}

		return $value;
	}

}
