<?php

namespace NM_Pdf\Objects;

defined( 'ABSPATH' ) || exit;

abstract class BaseObject implements \NM_Pdf\Interfaces\BaseObject {

	public $pdf_type;
	public $title;
	public $object_id;

	/**
	 * The object to create a pdf for
	 * Usually a post type such as order, post, product
	 * @param array $data Properties of the Object
	 */
	public function __construct( $data = [] ) {
		$this->set_data( $data );

		if ( empty( $this->title ) && !empty( $this->get_pdf_type() ) ) {
			$this->set_title( $this->get_default_title() );
		}
	}

	public function set_data( array $data ) {
		if ( !empty( $data ) ) {
			foreach ( array_keys( get_object_vars( $this ) ) as $key ) {
				if ( array_key_exists( $key, $data ) ) {
					$value = $data[ $key ];
					if ( is_callable( array( $this, "set_$key" ) ) ) {
						$this->{"set_$key"}( $value );
					} else {
						$this->{$key} = $value;
					}
				}
			}
		}
	}

	/**
	 * Check if this object is valid
	 */
	abstract public function is_valid();

	public function get_object_id() {
		return $this->object_id;
	}

	public function set_title( $title ) {
		$this->title = $title;
	}

	/**
	 * Get the object's title
	 */
	public function get_title() {
		return $this->title;
	}

	public function get_default_title() {
		return $this->get_pdf_types()[ $this->get_pdf_type() ][ 'title' ] ?? null;
	}

	/**
	 * Get the filename used to save the pdf when created
	 */
	public function get_pdf_filename() {
		$filename = $this->get_pdf_type() . '-' . $this->get_object_id() . '.pdf';
		return sanitize_file_name( apply_filters( 'nm_pdf_filename', $filename, $this ) );
	}

	public function get_pdf_type() {
		return $this->pdf_type;
	}

	public function set_pdf_type( $pdf_type ) {
		$this->pdf_type = $pdf_type;
	}

	/**
	 * Get the basename (not filepath) of the template file used to generate the pdf.
	 * Basename should be without the extension
	 * e.g. default (instead of default.php)
	 */
	public function get_template_name() {
		return apply_filters( 'nm_pdf_template_name', 'default', $this );
	}

	public function get_site_identity() {
		$name = get_bloginfo( 'name' );
		if ( has_custom_logo() ) {
			return wp_get_attachment_image( get_theme_mod( 'custom_logo' ), 'full', false, [ 'alt' => $name ] );
		}
		return $name;
	}

	public function get_header() {
		return nm_pdf()->get_option( 'header', null );
	}

	public function get_footer() {
		return nm_pdf()->get_option( 'footer', null );
	}

	public function get_custom_css() {
		return nm_pdf()->get_option( 'custom_css', null );
	}

	public function filter_value( $value, $key, ...$args ) {
		return $value;
	}

	public function get_value( $key, ...$args ) {
		$value = null;
		if ( is_callable( array( $this, "get_$key" ) ) ) {
			$value = $this->{"get_$key"}( ...$args );
		} elseif ( isset( $this->{$key} ) ) {
			$value = $this->{$key};
		}

		$filtered = $this->filter_value( $value, $key, ...$args );

		return apply_filters( 'nm_pdf_template_value', $filtered, $key, $args, $this );
	}

	public function get_document( $content ) {
		ob_start();
		?>
		<!DOCTYPE html>
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				<title><?php echo esc_html( $this->get_value( 'title' ) ); ?></title>
				<?php
				$style_file = $this->get_object_type() . '/' . $this->get_template_name() . '-style';
				echo wp_kses(
					nm_pdf()->get_wc_template( $style_file, [ 'object' => $this ] ),
					[ 'style' => true ]
				);

				$custom_css = $this->get_value( 'custom_css' );
				echo $custom_css ? '<style id="custom_css">' . wp_kses( $custom_css, [ 'style' => true ] ) . '</style>' : '';
				?>
			</head>
			<body><?php echo wp_kses_post( $content ); ?></body>
		</html>
		<?php
		return ob_get_clean();
	}

	public function get() {
		if ( $this->is_valid() ) {
			$template = $this->get_object_type() . '/' . $this->get_template_name();
			return $this->get_document( nm_pdf()->get_wc_template( $template, [ 'object' => $this ] ) );
		}
	}

	public function show_html() {
		if ( $this->is_valid() ) {
			echo $this->get();
		}
	}

	public function download_pdf() {
		if ( $this->is_valid() ) {
			$pdf = nm_pdf()->pdf();
			$pdf->set_content( $this->get() );
			$pdf->set_filename( $this->get_pdf_filename() );
			$pdf->download();
		}
	}

	public function show_pdf() {
		if ( $this->is_valid() ) {
			$pdf = nm_pdf()->pdf();
			$pdf->set_content( $this->get() );
			$pdf->set_filename( $this->get_pdf_filename() );
			$pdf->show();
		}
	}

}
