<?php

namespace NM_Pdf\Objects;

defined( 'ABSPATH' ) || exit;

class Wishlist extends BaseObject {

	public $wishlist_id;
	public $wishlist;

	public function __construct( $data = [] ) {
		parent::__construct( $data );
		$this->wishlist_id = $this->object_id;
	}

	public function is_valid() {
		return is_a( $this->get_wishlist(), '\NMGR_Wishlist' );
	}

	public function get_object_type() {
		return 'nm_gift_registry';
	}

	public function get_pdf_types() {
		return apply_filters( 'nm_pdf_object_pdf_types',
			[
				'print' => [
					'title' => sprintf(
						/* translators: %s: wishlist type title */
						__( 'Print %s', 'nm-pdf' ),
						nmgr_get_type_title()
					),
				]
			],
			$this
		);
	}

	public function get_wishlist() {
		if ( !$this->wishlist && function_exists( 'nmgr_get_wishlist' ) ) {
			$this->wishlist = nmgr_get_wishlist( $this->wishlist_id );
		}
		return $this->wishlist;
	}

	public function get_wishlist_items_template() {
		add_filter( 'nmgr_item_view_parts', [ $this, 'setup_item_view_parts' ], 0 );
		add_filter( 'nmgr_items_table_header_content', [ $this, 'setup_item_view_parts_header_labels' ], 10, 2 );
		add_filter( 'nmgr_get_template_args', [ $this, 'remove_product_title_link' ] );

		return nmgr_get_items_template( [
			'id' => $this->get_wishlist()->get_id(),
			'editable' => false
			] );
	}

	public function setup_item_view_parts( $parts ) {
		foreach ( $parts as $key => $part ) {
			if ( in_array( $part, [ 'action_buttons', 'checkbox', 'favourite', 'add_to_cart_button' ] ) ) {
				unset( $parts[ $key ] );
			}
		}
		return $parts;
	}

	public function setup_item_view_parts_header_labels( $html, $part ) {
		switch ( $part ) {
			case 'quantity':
				$html = __( 'Quantity', 'nm-gift-registry' );
				break;
			case 'purchased_quantity':
				$html = __( 'Purchased quantity', 'nm-gift-registry' );
				break;
			case 'favourite':
				$html = __( 'Favourite', 'nm-gift-registry' );
				break;
		}
		return $html;
	}

	public function remove_product_title_link( $args ) {
		if ( 'account/items/item-title.php' === $args[ 'template_name' ] ) {
			$args[ 'args' ][ 'product_link' ] = '';
		}
		return $args;
	}

}
