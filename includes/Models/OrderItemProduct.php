<?php

namespace NM_Pdf\Models;

defined( 'ABSPATH' ) || exit;

/**
 * Utility class to get value from Order item product class.
 */
class OrderItemProduct {

	private $item;
	private $product;

	public function __construct( \WC_Order_Item_Product $item ) {
		$this->item = $item;
		$this->product = $item->get_product();
	}

	public function get_value( $item_key ) {
		$value = '';

		if ( is_callable( [ $this->item, "get_$item_key" ] ) ) {
			$value = $this->item->{"get_$item_key"}();
		} elseif ( false !== strpos( $item_key, 'product_' ) ) {
			$new_key = str_replace( 'product_', '', $item_key );
			if ( is_callable( [ $this->product, "get_$new_key" ] ) ) {
				$value = $this->product->{"get_$new_key"}();
			}
		}

		return $this->get_custom_value( $value, $item_key );
	}

	private function get_custom_value( $value, $item_key ) {
		switch ( $item_key ) {
			case 'subtotal':
				$value = $this->item->get_order()->get_formatted_line_subtotal( $this->item );
				break;

			case 'product_weight':
				$value = $value ? wc_format_weight( $value ) : '&mdash;';
				break;
		}

		return $value;
	}

}
