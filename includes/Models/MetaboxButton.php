<?php

namespace NM_Pdf\Models;

use NM_Pdf\Button;

defined( 'ABSPATH' ) || exit;

class MetaboxButton {

	private $object;
	private $button;

	public function __construct( \NM_Pdf\Objects\BaseObject $object, $pdf_type = '' ) {
		$this->object = $object;
		$this->object->set_pdf_type( $pdf_type );
		$this->object->set_title( $this->object->get_default_title() );
		$this->button = new Button();
		$this->compose();
	}

	public function compose() {
		$this->button->set_url_args( [
			'object_id' => $this->object->object_id,
			'object_type' => $this->object->get_object_type(),
			'pdf_type' => $this->object->get_pdf_type(),
		] );

		$this->button->set_text( $this->object->get_title() );
	}

	public function get_button() {
		return $this->button;
	}

	public function get() {
		return $this->get_button()->get();
	}

}
