<?php

namespace NM_Pdf;

use NmeriMedia\V1\Settings as NM_Settings;

defined( 'ABSPATH' ) || exit;

class Settings extends NM_Settings {

	public $is_licensed = true;
	public $is_woocommerce_screen = true;
	public $sidebar_links = [
		'docs' => '',
		'review' => '',
		'get_pro' => '',
		'support' => '',
	];

	public function get_tabs() {
		return array(
			'general' => array(
				'tab_title' => __( 'General', 'nm-pdf' ),
				'sections_title' => sprintf(
					/* translators: %s Plugin name */
					__( '%s Settings', 'nm-pdf' ),
					$this->name
				),
				'show_sections' => true,
				'sections' => array(
					'one' => __( 'One', 'nm-pdf' ),
				),
			)
		);
	}

	public function general_tab_sections() {
		$sections = array();

		$sections[ 'one' ] = array(
			'title' => '',
			'description' => '',
			'section' => 'one',
			'fields' => array(
				array(
					'label' => __( 'How to access created PDF', 'nm-pdf' ),
					'id' => 'pdf_method',
					'default' => 'show_pdf',
					'type' => 'select',
					'options' => [
						'show_pdf' => __( 'Show in browser', 'nm-pdf' ),
						'download_pdf' => __( 'Download', 'nm-pdf' )
					],
				),
				array(
					'label' => __( 'Header', 'nm-pdf' ),
					'id' => 'header',
					'default' => '',
					'type' => 'textarea',
				),
				array(
					'label' => __( 'Footer', 'nm-pdf' ),
					'id' => 'footer',
					'default' => '',
					'type' => 'textarea',
					'desc_tip' => __( 'Terms and conditions, policies, etc.', 'nm-pdf' ),
				),
				array(
					'label' => __( 'Custom css', 'nm-pdf' ),
					'id' => 'custom_css',
					'default' => '',
					'type' => 'textarea',
					'desc_tip' => __( 'Add custom css without the "style" tag to change styles in the pdf document.', 'nm-pdf' ),
				),
			)
		);

		return $sections;
	}

	/**
	 * @todo Get all registered woocommerce emails
	 *
	 * This function is used to list all woocommerce emails for the user to select
	 * which emails he wants to attach the pdf invoice or packing-slip to.
	 *
	 * To use this function, add the array below to the settings
	 * array(
	  'label' => __( 'Send PDF with email', 'nm-pdf' ),
	  'id' => 'attach_to_mail',
	  'default' => '',
	  'type' => 'select',
	  'class' => 'wc-enhanced-select',
	  'custom_attributes' => array(
	  'style' => 'min-width:300px;',
	  'multiple' => 'multiple'
	  ),
	  'options' => $this->get_wc_emails(),
	  ),
	 *
	 * @return array
	 */
	public function get_wc_emails() {
		$filtered = [];
		if ( function_exists( 'wc' ) ) {
			$remove = [ 'customer_reset_password', 'customer_new_account' ];
			$emails = WC()->mailer()->get_emails();
			foreach ( $emails as $email ) {
				if ( is_object( $email ) && !in_array( $email->id, $remove ) ) {
					$filtered[ $email->id ] = $email->title;
				}
			}
		}
		return $filtered;
	}

}
