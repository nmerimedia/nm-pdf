<?php

namespace NM_Pdf;

use Dompdf\Dompdf;

require_once nm_pdf()->path . 'dompdf/autoload.inc.php';

defined( 'ABSPATH' ) || exit;

class NM_Dompdf implements Interfaces\Pdf {

	private $pdf;
	private $content;
	private $filename = 'document.pdf';

	public function __construct() {
		$this->pdf = new Dompdf();
		$this->pdf->set_option( 'isRemoteEnabled', true );
	}

	public function set_content( string $content ) {
		$this->content = $content;
	}

	public function set_filename( string $filename ) {
		$this->filename = $filename;
	}

	public function create() {
		$this->pdf->loadHtml( $this->content );
//		$this->pdf->setPaper( 'A4', 'landscape' );
		$this->pdf->render();
	}

	public function download() {
		$this->create();
		$this->pdf->stream( $this->filename );
	}

	public function show() {
		$this->create();
		echo $this->pdf->stream( $this->filename, [ 'Attachment' => 0 ] );
	}

}
