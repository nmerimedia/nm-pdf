<?php

namespace NM_Pdf;

defined( 'ABSPATH' ) || exit;

class Functions {

	/**
	 * Get the main class instance of the pdf library used to generate pdfs
	 * @return Object
	 */
	public static function get_pdf_class() {
		$classname = apply_filters( 'nm_pdf_classname', '\NM_Pdf\NM_Dompdf' );
		return new $classname();
	}

	public static function adapt_pdf( Interfaces\Pdf $pdf ) {
		return $pdf;
	}

	public static function pdf() {
		$pdf = nm_pdf()->get_pdf_class();
		return nm_pdf()->adapt_pdf( $pdf );
	}

	/**
	 * Get the object to create pdfs for
	 * @return \NM_Pdf\Objects\BaseObject[]
	 */
	public static function get_pdf_objects() {
		return apply_filters( 'nm_pdf_objects', [
			'shop_order' => '\NM_Pdf\Objects\Order',
			'nm_gift_registry' => '\NM_Pdf\Objects\Wishlist',
			] );
	}

	/**
	 * Create a pdf
	 * Uses parameters supplied in $_REQUEST
	 * @see \NM_Pdf\Button->url_args for full list of accepted parameters
	 */
	public static function create_pdf() {
		check_ajax_referer( 'nm_pdf_create' );

		$object_ids = array_filter( explode( ',', $_REQUEST[ 'object_id' ] ?? 0 ) );
		$object_class = nm_pdf()->get_pdf_objects()[ ($_REQUEST[ 'object_type' ] ?? null) ];
		$method = $_REQUEST[ 'method' ] ?? 'download_pdf';

		do_action( 'nm_pdf_create_before' );

		if ( $object_class ) {
			foreach ( $object_ids as $object_id ) {
				$object = new $object_class( [
					'pdf_type' => $_REQUEST[ 'pdf_type' ] ?? null,
					'object_id' => $object_id,
					] );

				if ( is_callable( [ $object, $method ] ) ) {
					$object->$method();
				}
			}
		}

		do_action( 'nm_pdf_create_after' );

		wp_die();
	}

}
