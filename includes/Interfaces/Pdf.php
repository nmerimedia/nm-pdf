<?php

namespace NM_Pdf\Interfaces;

defined( 'ABSPATH' ) || exit;

interface Pdf {

	public function set_content( string $content );

	public function set_filename( string $filename );

	public function download();

	public function show();

}
