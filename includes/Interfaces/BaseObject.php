<?php

namespace NM_Pdf\Interfaces;

defined( 'ABSPATH' ) || exit;

interface BaseObject {

	/**
	 * The type of object
	 */
	public function get_object_type();

	/**
	 * Types of pdf that can be created for this object
	 * @return array
	 */
	public function get_pdf_types();

	/**
	 * The filename of the pdf
	 * @return string
	 */
	public function get_pdf_filename();

	/**
	 * Get the pdf template content
	 * @return string
	 */
	public function get();

	/**
	 * Show the html of the pdf in the browser
	 * Pdf method
	 * @return void
	 */
	public function show_html();

	/**
	 * Download the pdf
	 * Pdf method
	 * @return void
	 */
	public function download_pdf();

	/**
	 * Show the pdf in the browser
	 * Pdf method
	 * @return void
	 */
	public function show_pdf();

}
