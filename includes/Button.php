<?php

namespace NM_Pdf;

/**
 * Compose the button for creating a pdf
 */
class Button {

	public $text;
	public $url_args = [
		'action' => 'nmpdf',
		'pdf_type' => '',
		'object_id' => '',
		'object_type' => '',
		'method' => 'show_html',
	];
	public $attributes = [
		'href' => '',
		'target' => '_blank',
		'class' => [
			'button',
			'nm-pdf',
			'create',
		],
	];
	public $icon_args = [
		'icon' => 'nm-pdf-printer',
		'sprite' => false,
		'fill' => '#999',
		'class' => [],
		'style' => null,
	];

	/**
	 * Compose the button for creating a pdf
	 * @param array $url_args Arguments used to compose the url query string for the button
	 */
	public function __construct( $url_args = [] ) {
		$this->set_url_args( $url_args );
	}

	/**
	 * Set the arguments used to compose the button href url
	 * @param array $args
	 */
	public function set_url_args( $args ) {
		$this->url_args = array_merge( $this->url_args, $args );
	}

	/**
	 * Set the button attributes
	 * @param array $attr
	 */
	public function set_attributes( $attr ) {
		$this->attributes = nm_pdf()->merge_args( $this->attributes, $attr );
	}

	/**
	 * Set the button text
	 * @param string $text
	 */
	public function set_text( $text ) {
		$this->text = $text;
	}

	/**
	 * Get the arguments used to compose the button href url value
	 * @return array
	 */
	public function get_url_args() {
		$this->url_args[ 'method' ] = nm_pdf()->get_option( 'pdf_method', $this->url_args[ 'method' ] );
		return apply_filters( 'nm_pdf_button_url_args', $this->url_args, $this );
	}

	/**
	 * Get the url base where the button links to for the pdf to be generated
	 * @return string
	 */
	public function get_url_base() {
		return apply_filters( 'nm_pdf_buton_url_base', admin_url( 'admin-ajax.php' ), $this );
	}

	/**
	 * Get the href url value for the button
	 * @return string
	 */
	public function get_url() {
		return wp_nonce_url( add_query_arg( $this->get_url_args(), $this->get_url_base() ), 'nm_pdf_create' );
	}

	/**
	 * Get the html attributes used to compose the button
	 * @return array
	 */
	public function get_attributes() {
		$this->attributes[ 'href' ] = $this->get_url();
		$this->attributes[ 'class' ][] = $this->get_url_args()[ 'pdf_type' ] ?? null;

		if ( 'download_pdf' === $this->get_url_args()[ 'method' ] ) {
			$this->attributes[ 'target' ] = '_self';
		}
		return apply_filters( 'nm_pdf_button_attributes', $this->attributes, $this );
	}

	/**
	 * Get the button text
	 * @return string
	 */
	public function get_text() {
		return apply_filters( 'nm_pdf_button_text', $this->text, $this );
	}

	/**
	 * Get the arguments for composing the button svg icon
	 * @return array
	 */
	public function get_icon_args() {
		if ( $this->get_text() ) {
			$this->icon_args[ 'class' ] = array_merge( ($this->icon_args[ 'class' ] ?? [] ), [ 'align-with-text' ] );
			$this->icon_args[ 'style' ] = $this->icon_args[ 'style' ] ?? 'margin-right:6px;';
		}
		return apply_filters( 'nm_pdf_icon_args', $this->icon_args, $this );
	}

	/**
	 * Get the svg icon used in the button html
	 * @return string
	 */
	public function get_icon() {
		return nm_pdf()->get_svg( $this->get_icon_args() );
	}

	/**
	 * Get the html of the button
	 * @return string
	 */
	public function get() {
		$attr = nm_pdf()->format_attributes( $this->get_attributes() );
		return '<a ' . $attr . '>' . $this->get_icon() . $this->get_text() . '</a>';
	}

}
