/**
 * Define a common blockUI block setup for plugin
 *
 * @param {string | jQuery object} selector The element to be blocked
 */
function block(selector) {
	if ($.blockUI) {
		$(selector).addClass('is-blocked').block({
			message: null,
			overlayCSS: {
				backgroundColor: '#fff'
			},
			css: {
				backgroundColor: 'transparent',
				border: 'none'
			}
		});
	}
}

function unblock(selector) {
	if ($.blockUI) {
		$(selector).removeClass('is-blocked').unblock();
	}
}

/**
 * Show a dialog template
 * @param {string} template Template to show in a dialog
 * @param {object} args Arguments used to compose the template. "component" key is
 * required with a value.
 * @returns {undefined}
 */
function showTemplate(template, args) {
	if (!args.component) {
		console.warn('showTemplate: The "component" key must be present in the object supplied as the second argument to the function and this key must have the value of a component such as "modal" or "offcanvas".');
		return;
	}
	var d = new DOMParser().parseFromString(template, 'text/html');
	var id = d.querySelector('body').firstChild.getAttribute('id');

	// Remove previous identitcal components from dom
	var prevComps = document.querySelectorAll('#' + id);
	if (prevComps.length) {
		prevComps.forEach(function (c) {
			c.remove();
		});
	}

	// Create and display component
	document.body.appendChild(d.querySelector('body').children[0]);
	var el = document.getElementById(id);
	var comp = new NM[args.component](el);
	comp.show();
}