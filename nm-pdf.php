<?php

/**
 * Plugin Name: NM PDF
 * Plugin URI: https://nmerimedia.com
 * Description: Create pdf invoices and packing-slips from woocommerce orders  quickly and easily. <a href="https://nmerimedia.com/product-category/plugins/" target="_blank">See more plugins&hellip;</a>
 * Author: Nmeri Media
 * Author URI: https://nmerimedia.com
 * Domain Path: /languages
 * Version: 0.3
 * Requires at least: 4.7
 * WC tested up to: 5.9.0
 */
use NM_Pdf\Functions;

defined( 'ABSPATH' ) || exit;

if ( !class_exists( 'NmeriMedia\V1\Autoload' ) ) {
	require_once 'nmerimedia/v1/Autoload.php';
}

NmeriMedia\V1\Autoload::$namespaces[ 'NM_Pdf' ] = __DIR__ . '/includes';
NmeriMedia\V1\Autoload::run();

class NM_Pdf extends Functions {

	use \NmeriMedia\V1\Traits\Instance,
		 \NmeriMedia\V1\Traits\PluginProps,
		 \NmeriMedia\V1\Traits\Utils;

	/**
	 *
	 * @var \NM_Pdf\Settings
	 */
	public $settings;

	public function __construct() {
		self::set_plugin_props( __FILE__ );
	}

	public function run() {
		add_action( 'woocommerce_init', [ $this, 'maybe_install_and_run' ] ); // init hook
		add_action( 'admin_init', [ $this, 'show_inactive_notice' ] );
		register_uninstall_hook( __FILE__, [ __CLASS__, 'uninstall' ] );
	}

	public function show_inactive_notice() {
		if ( !function_exists( 'wc' ) ) {
			add_filter( 'plugin_action_links_' . $this->basename, [ $this, 'inactive_plugin_action_links' ] );
		}
	}

	public function install_actions() {
		$this->settings->save_default_values();
		update_option( 'nm_pdf_version', $this->version );
		do_action( 'nm_pdf_installed' );
	}

	/**
	 * @return \NmeriMedia\V1\Settings
	 */
	public function get_settings() {
		if ( !$this->settings ) {
			$this->settings = new NM_Pdf\Settings( 'nm_pdf' );
		}

		return $this->settings;
	}

	public static function get_plugin_fn() {
		return nm_pdf();
	}

	public function maybe_install_and_run() {
		if ( !did_action( 'woocommerce_init' ) ) {
			return;
		}

		$this->get_settings();

		// Install plugin
		if ( version_compare( get_option( 'nm_pdf_version' ), $this->version, '<' ) ) {
			$this->install_actions();
		}

		add_action( 'init', [ $this, 'load_plugin_textdomain' ] );

		// Show plugin links
		add_filter( 'plugin_action_links_' . $this->basename, [ $this, 'plugin_action_links' ] );
		add_filter( 'plugin_row_meta', [ $this, 'plugin_row_meta' ], 10, 2 );


		// Run plugin
		add_filter( 'is_nmerimedia_screen', [ $this, 'is_nmerimedia_screen' ] );
		add_action( 'admin_footer', [ $this, 'enqueue_sprite_file' ] );
		add_filter( 'nmerimedia_svg_args', [ $this, 'filter_svg_args' ] );
		add_action( 'wp_ajax_nmpdf', [ 'NM_Pdf', 'create_pdf' ] );

		NM_Pdf\Filters::run();

		do_action( 'nm_pdf_init' ); // after 'woocommerce_init'
	}

	public function load_plugin_textdomain() {
		load_plugin_textdomain( 'nm-pdf', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
	}

	public static function uninstall() {
		global $wpdb;
		$wpdb->query( "DELETE FROM $wpdb->options WHERE option_name LIKE 'nm_pdf%';" );
	}

	public function inactive_plugin_action_links( $links ) {
		$notice = esc_attr__( 'This plugin requires WooCommerce to be activated for it to run.', 'nm-pdf' );

		return array_merge( $links, [
			'<span style="color:#ffb900; cursor:pointer;" onclick="alert(\'' . $notice . '\');">' .
			__( 'Not active *', 'nm-pdf' ) .
			'</span>'
			] );
	}

	public function plugin_action_links( $links ) {
		return array_merge( $links, [
			'<a href="' . menu_page_url( 'nm-pdf', false ) . '">' .
			__( 'Settings', 'nm-pdf' ) .
			'</a>'
			] );
	}

	public function plugin_row_meta( $links, $file ) {
		if ( $file == $this->basename ) {
			$links[] = '<a target="_blank" href="https://docs.nmerimedia.com">' . __( 'Docs', 'nm-pdf' ) . '</a>';
		}
		return $links;
	}

	public function get_option( $field_key = '', $default_value = null ) {
		return $this->settings->get_option( $field_key, $default_value );
	}

	public function is_nmerimedia_screen( $bool ) {
		if ( in_array( nm_pdf()->get_current_screen_id(), [
				'shop_order',
				'edit-shop_order'
			] ) ) {
			$bool = true;
		}
		return $bool;
	}

	public function include_admin_assets_in_current_page() {
		$include = false;
		$screen_ids = apply_filters( 'nm_pdf_include_admin_assets_in_current_page', [
			'shop_order',
			'edit-shop_order'
			] );

		foreach ( $screen_ids as $screen_id ) {
			if ( $screen_id === nm_pdf()->get_current_screen_id() ) {
				$include = true;
				break;
			}
		}

		return $include;
	}

	public function enqueue_sprite_file() {
		if ( $this->include_admin_assets_in_current_page() ) {
			self::include_sprite_file();
		}
	}

	public function filter_svg_args( $args ) {
		if ( false !== strpos( $args[ 'icon' ], $this->slug ) ) {
			$args[ 'id' ] = $args[ 'icon' ];
			$args[ 'icon' ] = str_replace( "$this->slug-", '', $args[ 'icon' ] );
			$args[ 'class' ] = array_merge( [ $this->slug ], ( array ) $args[ 'class' ] );
		}
		return $args;
	}

}

/**
 * @return NM_Pdf
 */
function nm_pdf() {
	return NM_Pdf::get_instance();
}

nm_pdf()->run();
