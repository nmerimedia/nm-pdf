=== NM PDF ===
Contributors: nmerii
Tags: pdf, woocommerce, woocommerce pdf, invoice, packing slips, print, download, delivery note, customer note, export, email
Requires at least: 4.7
Tested up to: 5.8
Requires PHP: 7.0
Stable tag: 1.0.0


== Description ==

Create pdf invoices and packing-slips from woocommerce orders quickly and easily.

Features:

* Create pdf invoices for woocommerce orders
* Create pdf packing-slips for woocommerce orders
* Download or view pdf files in the browser
* Attach pdfs to emails automatically
* Set custom pdf filenames
* Customize invoice numbers
* Customize pdf templates
* Sequential invoice numbers


== Installation ==

Simply install and activate as any normal plugin.


== Changelog ==

= 1.0.0 =
* Initial release.
