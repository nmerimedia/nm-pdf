<?php
defined( 'ABSPATH' ) || exit;

/**
 * Order items
 */
$line_items = $object->get_value( 'order_items' );
$items_data = $object->get_value( 'order_items_data' );

if ( !empty( $line_items ) ) :

	do_action( 'nm_pdf_template_before_order_items', $object );
	?>
	<section class="order-items-section">
		<table class="order-items w-100">
			<thead>
				<tr>
					<?php foreach ( $items_data as $header ) : ?>
						<th class="p-10"><?php echo esc_html( $header ); ?></th>
					<?php endforeach; ?>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ( $line_items as $item ) :
					$item_model = $object->get_item_model( $item );
					?>
					<tr>
						<?php foreach ( array_keys( $items_data ) as $item_key ) : ?>
							<td class="p-10 border-bottom <?php echo esc_attr( $item_key ); ?>">
								<?php
								do_action( 'nm_pdf_template_before_order_item_value', $item_key, $item, $object );
								echo wp_kses_post( apply_filters( 'nm_pdf_template_order_item_value',
										$item_model->get_value( $item_key ),
										$item_key,
										$item,
										$object
								) );

								if ( 'name' === $item_key && $item_meta = $object->get_value( 'item_meta', $item ) ) {
									?>
									<table class="item-meta">
										<?php foreach ( $item_meta as $meta_key => $meta_value ) : ?>
											<tr>
												<th><?php echo wp_kses_post( $meta_key ); ?>:</th>
												<td><?php echo wp_kses_post( force_balance_tags( $meta_value ) ); ?></td>
											</tr>
										<?php endforeach; ?>
									</table>
									<?php
								}

								do_action( 'nm_pdf_template_after_order_item_value', $item_key, $item, $object );
								?>
							</td>
						<?php endforeach; ?>
					</tr>
				<?php endforeach; // line item    ?>
			</tbody>
			<?php
			/**
			 * Order item totals
			 */
			$item_totals = $object->get_value( 'order_item_totals' );
			if ( $item_totals ) {
				?>
				<tfoot class="order-item-totals">
					<?php
					foreach ( $item_totals as $key => $total ) {
						?>
						<tr class="<?php echo sanitize_html_class( $key ); ?>">
							<th class="p-10 text-right" scope="row" colspan="<?php echo count( $items_data ) - 1; ?>">
								<?php echo wp_kses_post( $total[ 'label' ] ); ?>
							</th>
							<td class="p-10"><?php echo wp_kses_post( $total[ 'value' ] ); ?></td>
						</tr>
						<?php
					}
					?>
				</tfoot>
				<?php
			}
			?>
		</table>
	</section>
	<?php
	do_action( 'nm_pdf_template_after_order_items', $object );

	endif;