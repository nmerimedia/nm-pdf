<?php defined( 'ABSPATH' ) || exit; ?>

<style>

	body {
		font-size: 10pt;
	}

	table {
		border-collapse: collapse;
	}

	ol,
	ul {
		list-style: none;
		margin: 0;
		padding: 0;
	}

	li,
	ul {
		margin-bottom: 0.75em;
	}

	p {
		margin: 0;
		padding: 0;
	}

	p + p {
		margin-top: 1.25em;
	}

	a {
		border-bottom: 1px solid;
		text-decoration: none;
	}

	.w-100 {
		width: 100%;
	}

	table.layout-fixed {
		table-layout: fixed;
	}

	section {
		margin-bottom: 40px;
	}

	table .va-top {
		vertical-align: top;
	}

	th {
		text-align: left;
	}

	.text-right {
		text-align: right;
	}

	table.order-items thead {
		background-color: black;
		color: white;
	}

	.p-10 {
		padding: 10px;
	}

	.border-bottom {
		border-bottom: 1px solid #ddd;
	}

	table.order-items td.product_image img {
		width: 30px;
		height: auto;
	}

	.order-item-totals .order_total td {
		font-weight: bold;
	}

	<?php do_action( 'nm_pdf_style', $object ); ?>

</style>
