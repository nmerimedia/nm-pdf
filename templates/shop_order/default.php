<?php
defined( 'ABSPATH' ) || exit;
?>

<div class="nmwpdf-template <?php echo sanitize_html_class( $object->pdf_type ); ?>">
	<?php
	echo wp_kses_post( wptexturize( $object->get_value( 'header' ) ) );

	do_action( 'nm_pdf_template_before', $object );


	// Site logo or title
	$site_id = $object->get_value( 'site_identity' );
	if ( $site_id ) :
		if ( false === strpos( $site_id, '<img' ) ) :
			?>
			<h1 class="site-title"> <?php echo wp_kses_post( $site_id ); ?> </h1>
		<?php else : ?>
			<div class="site-logo"><?php echo wp_kses_post( $site_id ); ?> </div>
		<?php
		endif;
	endif;



	// Title
	$title = $object->get_value( 'title' );
	if ( $title ) :
		?>
		<h2 class="pdf-title"> <?php echo esc_html( $title ); ?></h2>
		<?php
	endif;


	do_action( 'nm_pdf_template_before_order_details', $object );
	?>

	<section class="order-details-section">
		<table class="order-details layout-fixed w-100">
			<tbody>
				<tr class="va-top">

					<?php
					$billing_address = $object->get_value( 'billing_address' );
					if ( $billing_address ) :
						?>
						<td class="billing-address">
							<h3><?php echo esc_html( 'Billing Address', 'nm-pdf' ); ?></h3>
							<div><?php echo wp_kses_post( $billing_address ); ?></div>
						</td>
					<?php endif; ?>

					<?php
					$shipping_address = $object->get_value( 'shipping_address' );
					if ( $shipping_address ) :
						?>
						<td class="shipping-address">
							<h3><?php echo esc_html( 'Shipping Address', 'nm-pdf' ); ?></h3>
							<div><?php echo wp_kses_post( $shipping_address ); ?></div>
						</td>
					<?php endif; ?>

					<?php
					$order_data = $object->get_value( 'order_data' );
					if ( !empty( $order_data ) ) :
						?>
						<td class="order-data">
							<h3><?php echo esc_html( 'Order Details', 'nm-pdf' ); ?></h3>
							<table class="w-100">
								<tbody>
									<?php foreach ( $order_data as $key => $sod ) : ?>
										<tr class="<?php echo sanitize_html_class( $key ); ?>">
											<th><?php echo esc_html( $sod[ 'title' ] ?? null  ); ?></th>
											<td><?php echo esc_html( $sod[ 'value' ] ?? null  ); ?></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</td>
					<?php endif; ?>

				</tr>
			</tbody>
		</table>
	</section>

	<?php
	do_action( 'nm_pdf_template_after_order_details', $object );


	echo $object->get_order_items_template();


	// Customer note
	$note = $object->get_value( 'customer_note' );
	if ( $note ):
		do_action( 'nm_pdf_template_before_customer_note', $object );
		?>
		<section class="customer-note-section">
			<h4><?php esc_html_e( 'Customer note:', 'nm-pdf' ); ?></h4>
			<p><?php echo wp_kses_post( nl2br( wptexturize( $note ) ) ); ?></p>
		</section>
		<?php
		do_action( 'nm_pdf_template_after_customer_note', $object );
	endif;



	do_action( 'nm_pdf_template_after', $object );

	echo wp_kses_post( wptexturize( $object->get_value( 'footer' ) ) );
	?>
</div>


