<?php
defined( 'ABSPATH' ) || exit;
?>

<div class="nmwpdf-template <?php echo sanitize_html_class( $object->get_pdf_type() ); ?>">
	<?php
	echo wp_kses_post( wptexturize( $object->get_value( 'header' ) ) );


	// Site logo or title
	$site_id = $object->get_value( 'site_identity' );
	if ( $site_id ) :
		if ( false === strpos( $site_id, '<img' ) ) :
			?>
			<h1 class="site-title"> <?php echo wp_kses_post( $site_id ); ?> </h1>
		<?php else : ?>
			<div class="site-logo"><?php echo wp_kses_post( $site_id ); ?> </div>
		<?php
		endif;
	endif;


	if ( $object->get_wishlist()->get_title() ) {
		printf( '<h2 class="nmgr-title">%s</h2>', esc_html( $object->get_wishlist()->get_title() ) );
	}

	if ( $object->get_wishlist()->get_description() ) {
		printf( '<section class="nmgr-description">%s</section>', wp_kses_post( wpautop( $object->get_wishlist()->get_description() ) ) );
	}

	echo $object->get_wishlist_items_template();


	echo wp_kses_post( wptexturize( $object->get_value( 'footer' ) ) );
	?>
</div>


