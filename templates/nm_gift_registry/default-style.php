<?php defined( 'ABSPATH' ) || exit; ?>

<style>

	body {
		/*font-size: 10pt;*/
	}

	section {
		margin-bottom: 20px;
	}

	table {
		border-collapse: collapse;
		width: 100%;
	}

	thead {
		background-color: black;
		color: white;
	}

	th {
		text-align: left;
	}

	td {
		border-bottom: 1px solid #ddd;
	}

	td, th {
		padding: 10px;
	}

	td .edit{
		display: none;
	}

	fieldset {
		border: none;
		margin: 0;
		padding: 0;
	}

	.thumbnail img {
		width: 50px;
		height: auto;
	}


	ol,
	ul {
		list-style: none;
		margin: 0;
		padding: 0;
	}

	li,
	ul {
		margin-bottom: 0.75em;
	}

	p {
		margin: 0;
		padding: 0;
	}

	p + p {
		margin-top: 1.25em;
	}


	table.total .label {
		font-weight: bold;
	}

	<?php do_action( 'nm_pdf_style', $object ); ?>

</style>
