- create custom filename for pdf download
- save date packing-slip was generated.
- add header and footer
- test generating pdfs on refund orders to see if there is error with order->get_order_number()
- check phpcs
- allow custom css





AFTER PLUGIN SUBMISSION
- replace all wp_kses_post with nm_pdf()->utils()->kses_post.



SNippets
- remove invoice pdf type
- Use order date as invoice date
- set shop name
- add shop address
- set shop logo
- remove customer note
- set document title
- select paper format
