var gulp = require('gulp');
var del = require('del');
var plugins = require('gulp-load-plugins')();
var xPlugins = {
	groupmq: require('gulp-group-css-media-queries'),
	notifier: require('node-notifier')
};
const {rollup} = require('rollup');
const {terser} = require('rollup-plugin-terser');

/**
 * Gulp noop does nothing and is simply used to process decisions
 */

/**
 * When you want to export for production type the code below in the terminal
 * And when you are done change the environment to development
 */
// export NODE_ENV=production

var isDev = (process.env.NODE_ENV !== 'production');

var config = {
	stylesSrc: "assets/src/scss/*.scss",
	stylesDest: "assets/css/",
	scriptsSrc: ["assets/src/js/*.js", "!assets/src/js/_*.js"],
	scriptsDest: "assets/js",
	scriptsFiles: "assets/src/js/*.js",
	bootstrapScriptsSrc: 'assets/src/js/_bootstrap.js',
	bootstrapScriptsDest: 'assets/js/bootstrap.min.js'
};

function clear(cb) {
	return del([config.stylesDest, config.scriptsDest]);
}


function styles(cb) {
	gulp.src(config.stylesSrc)
			.pipe(plugins.plumber())
			.pipe(plugins.sass({
				indentType: 'tab',
				indentWidth: 1,
				outputStyle: 'expanded'
			})).on('error', plugins.sass.logError)
			.pipe(plugins.autoprefixer())
			.pipe(plugins.rename({suffix: '.min'}))
			.pipe(plugins.cleanCss())
			.pipe(gulp.dest(config.stylesDest))
			.pipe(plugins.notify({message: 'TASK: "styles" Completed!', onLast: true}));
	cb();
}

function scripts(cb) {
	gulp.src(config.scriptsSrc)
			.pipe(plugins.include())
			.pipe(plugins.beautify())
			.pipe(plugins.rename({suffix: '.min'}))
			.pipe(plugins.uglify())
			.pipe(gulp.dest(config.scriptsDest))
			.pipe(plugins.notify({message: 'TASK: "scripts" Completed!', onLast: true}));
	cb();
}

function watch(cb) {
	gulp.watch(config.stylesSrc, styles);
	gulp.watch(config.scriptsFiles, scripts);
	cb();
}



async function bootstrapScripts() {
	const bundle = await rollup({
		input: config.bootstrapScriptsSrc,
		external: ['@popperjs/core']
	});

	return bundle.write({
		banner: function () {
			return `/*! Bootstrap v5.0.2 | Modified for Nmeri Media */`;
		},
		file: config.bootstrapScriptsDest,
		name: 'NM',
		format: 'umd',
		globals: {
			'@popperjs/core': 'Popper'
		},
		plugins: [
			terser()
		]
	});
}
;



exports.clear = clear;
exports.styles = styles;
exports.scripts = scripts;
exports.bootstrapScripts = bootstrapScripts;
exports.clear_styles_scripts = gulp.series(clear, gulp.parallel(styles, scripts));
exports.styles_scripts = gulp.parallel(styles, scripts);
exports.default = gulp.series(gulp.parallel(styles, scripts), watch);
