<?php

namespace NmeriMedia\V1\Traits;

defined( 'ABSPATH' ) || exit;

/**
 * Core hooks to run on plugin instantiation
 */
trait CoreHooks {

	public function run_core_hooks() {
		add_action( 'init', [ $this, 'load_plugin_textdomain' ] );
		add_filter( 'plugin_action_links_' . $this->basename, [ $this, 'plugin_action_links' ] );
		add_filter( 'plugin_row_meta', [ $this, 'plugin_row_meta' ], 10, 2 );
	}

	public function load_plugin_textdomain() {
		load_plugin_textdomain( $this->slug, false, plugin_basename( dirname( $this->file ) ) . '/languages' );
	}

	public function plugin_action_links( $links ) {
		if ( method_exists( $this, 'get_settings_page_url' ) ) {
			$settings_page_url = $this->get_settings_page_url();
			if ( $settings_page_url ) {
				return array_merge( $links, [
					'<a href="' . $settings_page_url . '">' .
					__( 'Settings', 'nmerimedia' ) .
					'</a>'
					] );
			}
		}
		return $links;
	}

	public function plugin_row_meta( $links, $file ) {
		if ( $file == $this->basename ) {
			$array = [
				__( 'Docs', 'nmerimedia' ) => $this->docs_url,
				__( 'Support', 'nmerimedia' ) => $this->support_url,
				__( 'Review', 'nmerimedia' ) => $this->review_url,
			];

			foreach ( $array as $text => $url ) {
				if ( $url ) {
					$links[] = '<a target="_blank" href="' . $url . '">' . $text . '</a>';
				}
			}

			if ( !$this->is_pro && $this->product_url ) {
				$links[] = '<a target="_blank" href="' . $this->product_url . '" style="color:#b71401;"><strong>' . __( 'Get PRO', 'nmerimedia' ) . '</strong></a>';
			}
		}
		return $links;
	}

}
