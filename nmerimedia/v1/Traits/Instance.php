<?php

namespace NmeriMedia\V1\Traits;

defined( 'ABSPATH' ) || exit;

/**
 * Get the instance of the class
 */
trait Instance {

	private static $instance;

	/**
	 *
	 * @param string $filepath The filepath to the plugin base file
	 * 												 e.g /www/html/wordpress/wp-content/plugins/plugin/plugin.php
	 * @param array $props Properties or arguments to pass on to the class
	 * @return $this
	 */
	public static function get_instance( $filepath = null, $props = [] ) {
		if ( is_null( static::$instance ) ) {
			static::$instance = new static( $filepath, $props );
		}
		return static::$instance;
	}

}
