<?php

namespace NmeriMedia\V1\Traits;

defined( 'ABSPATH' ) || exit;

trait Core {

	use Instance,
		 Constructor,
		 PluginProps,
		 CoreHooks,
		 Utils;

}
