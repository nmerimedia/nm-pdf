<?php

namespace NmeriMedia\V1\Traits;

use NmeriMedia\V1\Utils as NM_Utils;

defined( 'ABSPATH' ) || exit;

/**
 * @class \NmeriMedia\V1\Traits\Scripts
 */
trait Scripts {

	/**
	 * Enqueue all scripts necessary for the admin area
	 */
	public function enqueue_all_admin() {
		$this->enqueue_select2();
		$this->enqueue_boostrap();
		$this->enqueue_admin();
	}

	/**
	 * Enqueue all scripts necessary for the frontend area
	 */
	public function enqueue_all_frontend() {
		$this->enqueue_select2();
		$this->enqueue_boostrap();
		$this->enqueue_frontend();
	}

	public function get_url( $path = '' ) {
		$url = NM_Utils::url();
		return $path ? $url . $path : $url;
	}

	public function get_version() {
		return NM_Utils::version();
	}

	public function get_prefix() {
		return 'nmerimedia-v1-';
	}

	public function get_prefixed_handle( $handle ) {
		return $this->get_prefix() . $handle;
	}

	public function get_styles() {
		$styles = [
			'select2' => [
				'src' => $this->get_url( 'assets/css/select2.min.css' ),
				'deps' => [],
				'version' => $this->get_version(),
			],
			'bootstrap' => [
				'src' => $this->get_url( 'assets/css/bootstrap.min.css' ),
				'deps' => [],
				'version' => '5.0.2',
			],
		];
		return $styles;
	}

	public function get_style( $handle ) {
		return $this->get_styles()[ $handle ] ?? null;
	}

	public function enqueue_style( $handle ) {
		$style = $this->get_style( $handle );
		if ( $style && !empty( $style[ 'src' ] ) ) {
			$prefixed_handle = $this->get_prefixed_handle( $handle );
			wp_enqueue_style( $prefixed_handle, $style[ 'src' ], $style[ 'deps' ] ?? [], $style[ 'version' ] ?? null  );
		}
	}

	public function register_style( $handle ) {
		$style = $this->get_style( $handle );
		if ( $style && !empty( $style[ 'src' ] ) ) {
			$prefixed_handle = $this->get_prefixed_handle( $handle );
			wp_register_style( $prefixed_handle, $style[ 'src' ], $style[ 'deps' ] ?? [], $style[ 'version' ] ?? null  );
		}
	}

	/**
	 * Enqueue select2 scripts and styles
	 */
	public function enqueue_select2() {
		$this->enqueue_select2_style();
		$this->enqueue_select2_script();
	}

	public function enqueue_select2_style() {
		wp_enqueue_style( 'nmerimedia-v1-select2',
			NM_Utils::url() . 'assets/css/select2.min.css',
			array(),
			NM_Utils::version()
		);
	}

	public function enqueue_select2_script() {
		wp_enqueue_script( 'nmerimedia-v1-select2',
			NM_Utils::url() . 'assets/js/select2.full.min.js',
			[ 'jquery' ],
			'4.0.3',
			true
		);
	}

	public function enqueue_boostrap() {
		$this->enqueue_bootstrap_style();
		$this->enqueue_bootstra_script();
	}

	public function enqueue_bootstrap_style() {
		wp_enqueue_style( 'nmerimedia-v1-bootstrap',
			NM_Utils::url() . 'assets/css/bootstrap.min.css',
			array(),
			'5.0.2'
		);
	}

	public function enqueue_bootstrap_script() {
		wp_enqueue_script( 'nmerimedia-v1-bootstrap',
			NM_Utils::url() . 'assets/js/bootstrap-nm.bundle.min.js',
			[ 'jquery' ],
			'5.0.2',
			true
		);
	}

	/**
	 * Enqueue custom scripts and styles for admin area
	 */
	public function enqueue_admin() {
		$this->enqueue_admin_style();
		$this->enqueue_admin_script();
	}

	public function enqueue_admin_style() {
		wp_enqueue_style( 'nmerimedia-v1-admin',
			NM_Utils::url() . 'assets/css/admin.min.css',
			array(),
			NM_Utils::version()
		);
	}

	public function enqueue_admin_script() {
		wp_enqueue_script( 'nmerimedia-v1-admin',
			NM_Utils::url() . 'assets/js/admin.min.js',
			[ 'jquery' ],
			NM_Utils::version(),
			true
		);
	}

	public function enqueue_frontend() {
		$this->enqueue_frontend_style();
	}

	public function enqueue_frontend_style() {
		wp_enqueue_style( 'nmerimedia-v1-frontend',
			NM_Utils::url() . 'assets/css/frontend.min.css',
			[],
			NM_Utils::version()
		);
	}

}
