<?php

namespace NmeriMedia\V1\Traits;

defined( 'ABSPATH' ) || exit;

/**
 * The properties of the plugin
 * @class \NmeriMedia\V1\Traits\PluginProps
 */
trait PluginProps {

	public $name;
	public $version;
	public $url;
	public $path;

	/**
	 * Plugin root filepath e.g /www/html/wordpress/wp-content/plugins/nm-plugin/nm-plugin.php
	 */
	public $file;

	/**
	 * Slug of plugin root file slug e.g nm-plugin
	 */
	public $slug;

	/**
	 * Basename of plugin root file e.g. nm-plugin/nm-plugin.php
	 */
	public $basename;

	/**
	 * Plugin base e.g nm_plugin
	 * Usually taken from plugin root file
	 */
	public $base;

	/**
	 * Whether the plugin is licensed
	 * @var boolean
	 */
	public $is_licensed = false;

	/**
	 * Pro version
	 * @var boolean
	 */
	public $is_pro = false;

	public function set_plugin_props( $filepath = null, $props = [] ) {
		$filedata = [];

		if ( !is_null( $filepath ) ) {
			if ( !function_exists( 'get_plugin_data' ) ) {
				require_once ABSPATH . 'wp-admin/includes/plugin.php';
			}

			$this->file = $filepath;
			$this->url = plugin_dir_url( $filepath );
			$this->path = plugin_dir_path( $filepath );
			$this->slug = pathinfo( $filepath, PATHINFO_FILENAME );
			$this->basename = plugin_basename( $filepath );

			$plugin_headers = array(
				'name' => 'Plugin Name',
				'version' => 'Version',
				'docs_url' => 'Nmerimedia Docs URI',
				'product_url' => 'Nmerimedia Product URI',
				'review_url' => 'Nmerimedia Review URI',
				'support_url' => 'Nmerimedia Support URI',
			);

			$filedata = get_file_data( $filepath, $plugin_headers );
		}

		$this->base = str_replace( '-', '_', $this->slug );

		$merged_props = array_merge( $filedata, ( array ) $props );

		foreach ( $merged_props as $key => $val ) {
			$this->{$key} = $val;
		}
	}

}
