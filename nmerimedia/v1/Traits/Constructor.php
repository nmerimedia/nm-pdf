<?php

namespace NmeriMedia\V1\Traits;

defined( 'ABSPATH' ) || exit;

/**
 * Class Constructor
 */
trait Constructor {

	/**
	 * Class Constructor
	 * @param string $filepath Plugin root filepath
	 * @param array $props Properties to set in the class
	 */
	public function __construct( $filepath = null, $props = [] ) {
		if ( method_exists( $this, 'set_plugin_props' ) ) {
			$this->set_plugin_props( $filepath, $props );
		}

		if ( method_exists( $this, 'run_core_hooks' ) ) {
			$this->run_core_hooks();
		}

		if ( method_exists( __CLASS__, 'uninstall' ) && $this->file ) {
			register_uninstall_hook( $this->file, [ __CLASS__, 'uninstall' ] );
		}
	}

}
