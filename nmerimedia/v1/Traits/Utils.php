<?php

namespace NmeriMedia\V1\Traits;

use \NmeriMedia\V1\ReadmeParser;

defined( 'ABSPATH' ) || exit;

/**
 * @class \NmeriMedia\V1\Traits\Utils
 */
trait Utils {

	/**
	 * Get the function containing the plugin properties
	 * @return object
	 */
	public static function get_plugin_fn() {
		return null;
	}

	/**
	 * Svg tags allowed by the plugin
	 *
	 * @return array
	 */
	public static function allowed_svg_tags() {
		return array(
			'svg' => array(
				'id' => true,
				'role' => true,
				'width' => true,
				'height' => true,
				'class' => true,
				'style' => true,
				'fill' => true,
				'xmlns' => true,
				'viewbox' => true,
				'aria-hidden' => true,
				'focusable' => true,
				'data-notice' => true, // may be deprecated soon. Used temporarily.
			),
			'use' => array(
				'xlink:href' => true
			),
			'title' => array(
				'data-title' => true
			),
			'path' => array(
				'fill' => true,
				'fill-rule' => true,
				'd' => true,
				'transform' => true,
			),
			'polygon' => array(
				'fill' => true,
				'fill-rule' => true,
				'points' => true,
				'transform' => true,
				'focusable' => true,
			),
		);
	}

	/**
	 * Get the allowed post tags
	 * This returns wordpress allowed html tags in post and the plugin's allowed svg tags
	 *
	 * @return array
	 */
	public static function allowed_post_tags() {
		return array_merge( wp_kses_allowed_html( 'post' ), static::allowed_svg_tags() );
	}

	/**
	 * Clean variables using sanitize_text_field. Arrays are cleaned recursively.
	 * Non-scalar values are ignored.
	 *
	 * @param string|array $var Data to sanitize.
	 * @return string|array
	 */
	public static function sanitize( $var ) {
		if ( is_array( $var ) ) {
			return array_map( array( __CLASS__, 'sanitize' ), $var );
		} else {
			return is_scalar( $var ) ? sanitize_text_field( $var ) : $var;
		}
	}

	/**
	 * Compose an svg icon using dynamic arguments provided
	 *
	 * @param string|array $args The name of the svg icon to get or array of icon parameters.
	 *
	 * If $args is an array, registered array keys needed to compose the svg are:
	 * - icon - [required] string The svg icon name. This should correspond to the name of an svg file in the assets/svg directory or the last part of the id of a symbol element in the sprite file - assets/svg/sprite.svg.
	 * - size - [optional] integer|string The svg icon width and height. (uses em unit if no unit is specified). Default 1em (16px).
	 * - class - [optional] array Classes to add to the svg icon.
	 * - sprite - [optional] boolean Whether to use the loaded svg sprite file (Default),
	 *            or to load the single svg icon from the assets/svg directory.
	 * - role - [optional] string Svg role attribute. Default img.
	 * - id - [optional|required] string Svg id attribute. Expected if "sprite" is true.
	 *        If not set, the 'icon' parameter is set as the id by default
	 * - style - [optional] string Svg Inline style.
	 * - title - [optional] string Svg title attribute.
	 * - fill - [optional] string Svg fill attribute.
	 * - path - [optional] Full path to the single svg icon file.
	 *   (Usually used if not using the loaded svg sprite file).
	 *
	 * @return string Svg HTML element
	 */
	public static function get_svg( $args ) {
		// Make sure icon name is given
		if ( !$args || (is_array( $args ) && (false === array_key_exists( 'icon', $args ))) ) {
			// If no arguments are set or the icon key is not in the array, require icon key
			return;
		} elseif ( is_string( $args ) ) {
			//if a string argument is given, assume it is the icon name and set it in the array
			$args = array( 'icon' => $args );
		}

		// Set defaults.
		$defaults = array(
			'id' => $args[ 'icon' ],
			'icon' => $args[ 'icon' ],
			'size' => 1,
			'class' => [
				$args[ 'icon' ],
				'nm-svg',
			],
			'sprite' => true,
			'role' => 'img',
			'path' => '',
		);

		// Parse args.
		$args_1 = apply_filters( 'nmerimedia_svg_args', wp_parse_args( $args, $defaults ), $args );

		$args = $args_1;

		// Make sure default classes are present
		$args[ 'class' ] = ( array ) $args[ 'class' ];
		foreach ( $defaults[ 'class' ] as $class ) {
			if ( !in_array( $class, $args[ 'class' ] ) ) {
				$args[ 'class' ][] = $class;
			}
		}

		// Make sure the size has units (default em)
		$size = is_numeric( $args[ 'size' ] ) ? "{$args[ 'size' ]}em" : $args[ 'size' ];

		// Get extra svg parameters set by user that are not in the default expected arguments
		// e.g 'style', 'title' and 'fill'
		$extra_params = array_diff_key( $args, $defaults );
		$extra_params_string = '';

		// extract the title attribute if it exists so that we can add it separately to the svg
		$title = '';
		if ( isset( $extra_params[ 'title' ] ) && !empty( $extra_params[ 'title' ] ) ) {
			$string = htmlspecialchars( wp_kses_post( $extra_params[ 'title' ] ) );
			$title = sprintf( "<title data-title='%s'>%s</title>", $string, $string );
			unset( $extra_params[ 'title' ] );
		}

		// Create new indexed array from extra svg parameters
		if ( !empty( $extra_params ) ) {
			$arr = array();
			foreach ( $extra_params as $key => $value ) {
				$arr[] = esc_attr( $key ) . '="' . esc_attr( $value ) . '"';
			}
			// Compose string from array of extra svg parameters
			$extra_params_string = implode( ' ', $arr );
		}

		// Compose svg with the given attributes
		$composed_svg = sprintf( '<svg role="%s" width="%s" height="%s" class="%s" %s ',
			esc_attr( $args[ 'role' ] ),
			esc_attr( $size ),
			esc_attr( $size ),
			implode( ' ', array_map( 'esc_attr', $args[ 'class' ] ) ),
			$extra_params_string );

		if ( $args[ 'sprite' ] ) {
			/**
			 * we are using a sprite file
			 */
			$svg = sprintf( '%s>%s<use xlink:href="#%s"></use></svg>',
				$composed_svg,
				$title,
				esc_html( $args[ 'id' ] )
			);
		} else {
			/**
			 *  we are using a single svg file
			 */
			// Get the svg file
			$svg = static::get_svg_file( $args[ 'icon' ], $args[ 'path' ] );

			// Remove width and heigh attributes from svg if exists as we are adding our own
			$svg = preg_replace( '/(width|height)="\d*"\s/', '', $svg );

			// Merge composed svg with original svg
			$svg = preg_replace( '/^<svg /', $composed_svg, trim( $svg ) );

			// Add title attribute if it exists
			$svg = str_replace( '</svg>', "$title</svg>", $svg );

			// Remove newlines & tabs.
			$svg = preg_replace( "/([\n\t]+)/", ' ', $svg );

			// Remove white space between SVG tags.
			$svg = preg_replace( '/>\s*</', '><', $svg );
		}

		$svg = apply_filters( 'nmerimedia_svg', $svg, $args );

		$instance = static::get_plugin_fn();

		if ( isset( $instance->base ) ) {
			$svg = apply_filters( $instance->base . '_svg', $svg, $args );
		}

		return $svg;
	}

	/**
	 * Get a single svg icon file unmodified from the icon directory
	 *
	 * @param string $icon_name The name of the icon e.g. user.
	 * @param string $path The full path to the icon file.
	 *
	 * @return string Icon html
	 */
	public static function get_svg_file( $icon_name, $path = '' ) {
		$iconfile = trailingslashit( $path ) . "{$icon_name}.svg";
		$instance = static::get_plugin_fn();

		if ( !$path && isset( $instance->path ) ) {
			$iconfile = trailingslashit( $instance->path ) . 'assets/svg/' . "{$icon_name}.svg";
		}

		if ( !file_exists( $iconfile ) ) {
			$iconfile = \NmeriMedia\V1\Utils::path() . 'assets/svg/' . "{$icon_name}.svg";
		}

		if ( file_exists( $iconfile ) ) {
			ob_start();
			include $iconfile;
			return ob_get_clean();
		}
		return false;
	}

	/**
	 * Return an array of html attributes as a string to be inserted directly into the html element
	 * @param array $attributes Attributes to flatten
	 * @return string
	 */
	public static function format_attributes( $attributes ) {
		$sanitized_attributes = array();

		if ( !empty( $attributes ) ) {
			foreach ( ( array ) $attributes as $key => $value ) {
				$val = is_array( $value ) ? implode( ' ', array_map( 'esc_attr', $value ) ) : $value;
				$sanitized_attributes[] = esc_attr( $key ) . '="' . esc_attr( $val ) . '"';
			}
		}
		return implode( ' ', $sanitized_attributes );
	}

	/**
	 * Return an array of html attributes as a string to be inserted directly into the html element
	 * (Alias of format_attributes())
	 * @param array $attributes Attributes to flatten
	 * @return string
	 */
	public static function flatten_attributes( $attributes ) {
		return static::format_attributes( $attributes );
	}

	/**
	 * Merge supplied values with default values
	 *
	 * This function doesn't use wp_parse_args because it doesn't merge recursively.
	 *
	 * @param array $defaults Default values
	 * @param array $args Supplied values
	 * @return array
	 */
	public static function merge_args( $defaults, $args ) {
		$merged = $defaults;
		foreach ( $args as $key => $val ) {
			if ( isset( $defaults[ $key ] ) ) {
				$merged[ $key ] = is_array( $defaults[ $key ] ) ? array_merge_recursive( $defaults[ $key ], ( array ) $val ) : $val;
			} else {
				$merged[ $key ] = $val;
			}
		}
		return $merged;
	}

	/**
	 * Get a help tip
	 *
	 * @param string $title The text to show as the tip.
	 * @param array $args Extra arguments supplied to help compose the help tip
	 * @return string
	 */
	public static function help_tip( $title, $args = [] ) {
		$defaults = array(
			'id' => 'nm-icon-info',
			'icon' => 'nm-icon-info',
			'class' => array( 'nm', 'align-with-text' ),
			'style' => 'margin-left:5px;',
			'data-bs-title' => $title,
			'fill' => '#666',
			'data-bs-toggle' => 'tooltip',
		);
		$merged = static::merge_args( $defaults, $args );
		$svg = static::get_svg( $merged );
		return $svg;
	}

	/**
	 * Include the sprite file used for the plugin
	 */
	public static function include_sprite_file() {
		$instance = static::get_plugin_fn();
		$sprite_file = isset( $instance->path ) ? $instance->path . 'assets/svg/sprite.svg' : '';
		if ( file_exists( $sprite_file ) ) {
			include_once $sprite_file;
		}
	}

	public static function include_base_sprite_file() {
		$sprite_file = \NmeriMedia\V1\Utils::path() . 'assets/svg/sprite.svg';
		if ( file_exists( $sprite_file ) ) {
			include_once $sprite_file;
		}
	}

	/**
	 * Check if it is an admin request
	 * @return boolean
	 */
	public static function is_admin_request() {
		$current_url = home_url( add_query_arg( null, null ) );
		$admin_url = strtolower( admin_url() );
		$referrer = strtolower( wp_get_referer() );

		// Check if this is a admin request. If true, it
		// could also be a AJAX request from the frontend.
		if ( 0 === strpos( $current_url, $admin_url ) ) {
			// Check if the user comes from a admin page.
			if ( 0 === strpos( $referrer, $admin_url ) ) {
				return true;
			} else {
				return !wp_doing_ajax();
			}
		} else {
			return false;
		}
	}

	/**
	 * Get the current screen id of a page being viewed in the admin area
	 * @return string|null
	 */
	public static function get_current_screen_id() {
		if ( function_exists( 'get_current_screen' ) ) {
			$screen = get_current_screen();
			return $screen ? $screen->id : null;
		}
	}

	/**
	 * Get all the admin screens registered as nmerimedia screens
	 * These screens would automatically have nmerimedia styles and scripts enqueued on them.
	 *
	 * @return array
	 */
	public static function get_nmerimedia_screens() {
		return apply_filters( 'nmerimedia_screens', [ static::get_screen_id() ] );
	}

	/**
	 * Get the checkbox switch used by the plugin
	 * @param array $args Arguments supplied to create the checkbox switch:
	 * - input_id - {string, required} The input id
	 * - input_name - {string} The input name
	 * - input_value - {mixed} The input value
	 * - input_class - {array} The input class
	 * - input_attributes = {array} Attributes to go into the input element
	 * - label_class - {array} The label class
	 * - label_attributes - {array} Attributes to go into the label element
	 * - label_text - {string} The label text
	 * - checked - {boolean} Whether the checkbox should be checked or not.
	 * @param boolean $echo Whether to echo the result. Default false
	 * @return string Html
	 */
	public static function get_toggle_switch( $args, $echo = false ) {
		$defaults = array(
			'input_id' => mt_rand(),
			'input_name' => '',
			'input_type' => 'checkbox',
			'input_value' => 1,
			'input_class' => array(),
			'input_attributes' => array(),
			'label_class' => array(),
			'label_attributes' => array(),
			'label_text' => '',
			'checked' => ''
		);

		$params = wp_parse_args( $args, $defaults );

		if ( isset( $params[ 'input_attributes' ][ 'disabled' ] ) &&
			$params[ 'input_attributes' ][ 'disabled' ] ) {
			$params[ 'label_class' ][] = 'disabled';
		}

		if ( isset( $params[ 'input_attributes' ][ 'readonly' ] ) &&
			$params[ 'input_attributes' ][ 'readonly' ] ) {
			$params[ 'label_class' ][] = 'readonly';
		}

		$label_class = implode( ' ', array_map( 'esc_attr', ( array ) $params[ 'label_class' ] ) );
		$input_class = implode( ' ', array_map( 'esc_attr', ( array ) $params[ 'input_class' ] ) );
		$input_attributes = static::format_attributes( $params[ 'input_attributes' ] );
		$label_attributes = static::format_attributes( $params[ 'label_attributes' ] );
		$checked = $params[ 'checked' ] ? " checked='checked'" : '';

		$switch = '<label class="nm-toggle-switch ' . $label_class . '"' . wp_kses( $label_attributes, [] ) . '>' .
			wp_kses( $params[ 'label_text' ], static::allowed_post_tags() ) .
			'<input type="' . esc_attr( $params[ 'input_type' ] ) . '"
								id="' . esc_attr( $params[ 'input_id' ] ) . '"
                value="' . esc_attr( $params[ 'input_value' ] ) . '"
								class="' . $input_class . '"
                name="' . esc_attr( $params[ 'input_name' ] ) . '"' .
			$checked . wp_kses( $input_attributes, [] ) . '/>
                <label for="' . esc_attr( $params[ 'input_id' ] ) . '"></label>
            </label>';

		if ( $echo ) {
			// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			echo $switch;
		} else {
			return $switch;
		}
	}

	/**
	 * Get a template file from the templates path using "wc_get_template" woocommerce function.
	 *
	 * This function searches the templates path in the theme folder before defaulting to
	 * the templates path in the plugin folder if it doesn't find the file.
	 * This way, It allows plugin templates to be overridden by copying them to the theme folder
	 * similar to the way woocommerce works.
	 *
	 * The default expected theme template path where overridden templates reside is: yourtheme/plugin-slug'
	 * where 'yourtheme' is the name of your theme and 'plugin-slug' is the slug of this plugin
	 *
	 * @param string $name Name of template file to get (prefixed with subfolder if it exists in a
	 * subfolder of the template path).
	 * @param array $args Variables to send to the template file.
	 * @param boolean $echo Whether to echo the result. Default false.
	 *
	 * @return string Template html
	 */
	public static function get_wc_template( $name, $args = array(), $echo = false ) {
		$instance = static::get_plugin_fn();
		$namep = false === strpos( $name, '.', -0 ) ? $name . '.php' : $name;
		$defaults = array(
			'template_name' => $namep,
			'args' => $args,
			'template_path' => trailingslashit( $instance->slug ?? '' ),
			'default_path' => ($instance->path ?? '') . 'templates/',
			'plugin_base' => $instance->base ?? 'nmerimedia',
		);

		$fargs = apply_filters( ($instance->base ?? 'nmerimedia') . '_get_template_args', $defaults );

		if ( $echo ) {
			wc_get_template(
				$fargs[ 'template_name' ],
				$fargs[ 'args' ],
				$fargs[ 'template_path' ],
				$fargs[ 'default_path' ]
			);
		} else {
			return wc_get_template_html(
				$fargs[ 'template_name' ],
				$fargs[ 'args' ],
				$fargs[ 'template_path' ],
				$fargs[ 'default_path' ]
			);
		}
	}

	/**
	 * Output a template file from the templates path using "wc_get_template" woocommerce function.
	 *
	 * @param string $name Name of template file to get (prefixed with subfolder if it exists in a
	 * subfolder of the template path).
	 * @param array $args Variables to send to the template file.
	 */
	public static function wc_template( $name, $args = array() ) {
		static::get_wc_template( $name, $args, true );
	}

	/**
	 * Get the html of an admin notice
	 * @param string $msg The message to display
	 * @param string $notice_type The type of notice (error, info, success)
	 * @param boolean $is_dismissible Whether the notice should be dismissible
	 * @return string
	 */
	public static function get_admin_notice( $msg, $notice_type = 'error', $is_dismissible = true ) {
		$dismiss = $is_dismissible ? 'is-dismissible' : '';
		return "<div class='$dismiss notice notice-{$notice_type}'>$msg</div>";
	}

	/**
	 * Get all the registered Nmeri Media plugins or a specific plugin
	 * @param string $plugin_slug The slug of a specific registered plugin to get
	 * @return array
	 */
	public static function get_plugins( $plugin_slug = null ) {
		$plugins = include \NmeriMedia\V1\Utils::path() . 'templates/plugins.php';
		return $plugin_slug ? ($plugins[ $plugin_slug ] ?? null) : $plugins;
	}

	/**
	 * Add an element to an array
	 * @param array $array The array to add the element to
	 * @param int|string $position The index position in the array to add the element to.
	 * 											 Can be a numeric index in the array or a string representing
	 * 											 a specific array key.
	 * @param mixed $insert The element to insert into the array. Can be a simple array element
	 * 											or an associative array.
	 * @return array The original array with the element added to it.
	 *
	 */
	public static function array_add( $array, $position, $insert ) {
		$pos = is_int( $position ) ? $pos : array_search( $position, array_keys( $array ) );
		$ins = is_array( $insert ) ? $insert : array( $insert );
		return array_merge( array_slice( $array, 0, $pos ), $ins, array_slice( $array, $pos ) );
	}

	/**
	 * Get a plugin's readme.txt file as a string
	 * @param string $filepath Full filepath to the readme.txt file.
	 * @return \NmeriMedia\V1\ReadmeParser
	 */
	public static function get_readme_file( $filepath = null ) {
		return new ReadmeParser( $filepath . 'readme.txt' );
	}

	public static function get_option( $field_key = '', $default_value = null ) {
		$instance = static::get_plugin_fn();
		return method_exists( $instance, 'get_settings' ) ?
			$instance->get_settings()->get_option( $field_key, $default_value ) :
			null;
	}

	public function get_component( $type, $args = [] ) {
		$classname = '\NmeriMedia\\V1\\' . ucfirst( $type );
		if ( class_exists( $classname ) ) {
			$class = new $classname( $args );
			return $class;
		}
	}

}
