<?php

namespace NmeriMedia\V1\Traits;

defined( 'ABSPATH' ) || exit;

/**
 * Get the main function of the plugin if it exists
 *
 * This allows us to have access to the plugin's instance, properties, and methods.
 */
trait PluginFn {

	/**
	 * The name of the plugin function
	 * @var string
	 */
	public $plugin_fn_name = '';

	/**
	 * Get the main function of the plugin if it exists.
	 *
	 * This allows us to have access to the plugin's instance, properties, and methods.
	 * @return object|null
	 */
	public function plugin_fn() {
		$fn = $this->plugin_fn_name;
		return is_callable( $fn ) ? $fn() : null;
	}

}
