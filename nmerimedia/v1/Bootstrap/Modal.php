<?php

namespace NmeriMedia\V1\Bootstrap;

defined( 'ABSPATH' ) || exit;

/**
 * @class \NmeriMedia\V1\Bootstrap\Modal
 */
class Modal extends Dialog {

	public $type = 'modal';
	public $container_attributes = array(
		'class' => array(
			'modal',
			'fade'
		),
	);
	public $footer_attributes = array(
		'class' => array(
			'modal-footer'
		),
	);
	public $header_attributes = array(
		'class' => array(
			'modal-header'
		),
	);
	public $body_attributes = array(
		'class' => array(
			'modal-body'
		),
	);
	public $title_class = [ 'modal-title' ];
	public $dialog_attributes = array(
		'class' => array(
			'modal-dialog',
		),
	);
	public $content_attributes = array(
		'class' => array(
			'modal-content'
		),
	);

	public function get_dialog_attributes( $formatted = false ) {
		return $formatted ? $this->format_attributes( $this->dialog_attributes ) : $this->dialog_attributes;
	}

	public function get_content_attributes( $formatted = false ) {
		return $formatted ? $this->format_attributes( $this->content_attributes ) : $this->content_attributes;
	}

	public function hide_animation() {
		unset( $this->container_attributes[ 'class' ][ array_search( 'fade', $this->container_attributes[ 'class' ] ) ] );
	}

	/**
	 * Make the modal backdrop static
	 * This prevents the modal from closing when clicking outside it
	 * @return NmeriMedia\V1\Modal
	 */
	public function make_backdrop_static() {
		$this->container_attributes[ 'data-bs-backdrop' ] = 'static';
		return $this;
	}

	/**
	 * Make the modal dialog where the content is scrollable
	 * @return NmeriMedia\V1\Modal
	 */
	public function make_scrollable() {
		$this->dialog_attributes[ 'class' ][] = 'modal-dialog-scrollable';
		return $this;
	}

	/**
	 * Vertically center the modal
	 * @return NmeriMedia\V1\Modal
	 */
	public function make_vertically_centered() {
		$this->dialog_attributes[ 'class' ][] = 'modal-dialog-centered';
		return $this;
	}

	/**
	 * Make the modal width small (300px)
	 * Default width is 500px
	 * @return NmeriMedia\V1\Modal
	 */
	public function make_small() {
		$this->dialog_attributes[ 'class' ][ 'width' ] = 'modal-sm';
		return $this;
	}

	/**
	 * Make the modal width large (800px)
	 * Default width is 500px
	 * @return NmeriMedia\V1\Modal
	 */
	public function make_large() {
		$this->dialog_attributes[ 'class' ][ 'width' ] = 'modal-lg';
		return $this;
	}

	/**
	 * Make the modal width extra large (1140px)
	 * Default width is 500px
	 * @return NmeriMedia\V1\Modal
	 */
	public function make_extra_large() {
		$this->dialog_attributes[ 'class' ][ 'width' ] = 'modal-xl';
		return $this;
	}

	/**
	 * Make the modal width fullscreen
	 * Default width is 500px
	 * @return NmeriMedia\V1\Modal
	 */
	public function make_fullscreen() {
		$this->dialog_attributes[ 'class' ][ 'width' ] = 'modal-fullscreen';
		return $this;
	}

}
