<?php

namespace NmeriMedia\V1\Bootstrap;

defined( 'ABSPATH' ) || exit;

/**
 * @class \NmeriMedia\V1\Bootstrap\Toast
 */
class Toast extends Component {

	public $type = 'toast';
	public $notice_type = '';
	public $hide_close_button = false;
	public $wrapper_attributes = array(
		'class' => array(
			'toast-container',
		),
		'aria-live' => 'polite',
		'aria-atomic' => 'true',
		'role' => 'status',
		'position' => 'left' // the toast position on the screen values are left, right, top, bottom
	);
	public $container_attributes = array(
		'class' => array(
			'toast',
		),
	);
	public $body_attributes = array(
		'class' => array(
			'toast-body'
		),
	);
	public $content_attributes = array(
		'class' => array(
			'toast-content'
		),
	);

	public function has_close_button_hidden() {
		return ( bool ) $this->hide_close_button;
	}

	/**
	 * Set a toast notice type to get.
	 * @param string $type The type of notice to get. Values are notice, error and success.
	 * Default is success.
	 * @param boolean $style Whether to style the toast according to the notice type. Default true.
	 */
	public function set_notice_type( $type, $style = true ) {
		$this->container_attributes[ 'class' ][] = $type;

		if ( 'error' === $type ) {
			$this->container_attributes[ 'aria-live' ] = 'assertive';
			$this->container_attributes[ 'role' ] = 'alert';
		} else {
			$this->container_attributes[ 'aria-live' ] = 'polite';
			$this->container_attributes[ 'role' ] = 'status';
		}

		if ( $style ) {
			switch ( $type ) {
				case 'notice':
					$br_color = '#1e85be';
					break;
				case 'error':
					$br_color = '#b81c23';
					break;

				default:
					$br_color = '#0f834d';
					break;
			}
			$color = 'rgba(0, 0, 0, 0.95)';
			$this->container_attributes[ 'style' ] = "border-left:4px solid $br_color;color:$color;";
		}
	}

	public function get_wrapper_attributes( $formatted = false ) {
		$default = array(
			'class' => array(
				'nm-component',
			)
		);
		$atts = $this->merge_args( $default, $this->wrapper_attributes );
		$atts[ 'style' ] = "{$atts[ 'position' ]}:0;";

		return $formatted ? $this->format_attributes( $atts ) : $atts;
	}

	public function get_wrapper( $echo = false ) {
		$attributes = $this->get_wrapper_attributes( true );

		if ( $echo ) {
			echo '<div ' . wp_kses( $attributes, [] ) . '></div>';
		} else {
			return "<div $attributes></div>";
		}
	}

}
