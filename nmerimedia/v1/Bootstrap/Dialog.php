<?php

namespace NmeriMedia\V1\Bootstrap;

defined( 'ABSPATH' ) || exit;

/**
 * @class \NmeriMedia\V1\Bootstrap\Dialog
 */
abstract class Dialog extends Component {

	public $title = '';
	public $title_tag = 'h4';
	public $header = '';
	public $footer = '';
	public $hide_header = false;
	public $hide_footer = false;
	public $hide_title = false;
	public $hide_header_close_button = false;
	public $header_attributes = [];
	public $footer_attributes = [];
	public $title_class = [];
	public $show_back_button = false;
	public $show_next_button = false;
	public $show_cancel_button = false;
	public $show_save_button = false;

	public function __construct( $args = [] ) {
		$this->container_attributes = $this->merge_args( [
			'class' => [ 'dialog' ],
			'aria-hidden' => true,
			'role' => 'dialog',
			'tabindex' => '-1',
			], $this->container_attributes );

		parent::__construct( $args );

		$this->setup_footer_buttons();
	}

	public function setup_footer_buttons() {
		$buttons = [ 'back_button', 'next_button', 'cancel_button', 'save_button' ];
		foreach ( $buttons as $button ) {
			if ( $this->{"show_$button"} ) {
				$this->footer .= $this->{"get_$button"}();
			}
		}
	}

	public function set_title( $title, $use_default_html = true ) {
		if ( $use_default_html ) {
			$class = implode( ' ', $this->get_title_class() );
			$tag = esc_attr( $this->get_title_tag() );
			$this->title = "<$tag class='$class'>$title</$tag>";
		} else {
			$this->title = $title;
		}
	}

	public function set_title_tag( $tag ) {
		$this->title_tag = $tag;
	}

	public function set_title_class( array $class, $replace = false ) {
		$this->title_class = $replace ? $class : array_unique( array_merge( $this->title_class, $class ) );
	}

	public function set_header( $header ) {
		$this->header = $header;
	}

	public function set_footer( $footer ) {
		$this->footer = $footer;
	}

	public function set_footer_attributes( $attributes, $replace = false ) {
		$this->set_attributes( 'footer', $attributes, $replace );
	}

	public function set_header_attributes( $attributes, $replace = false ) {
		$this->set_attributes( 'header', $attributes, $replace );
	}

	/**
	 * Set whether to hide the component header
	 *
	 * @param boolean $bool
	 */
	public function hide_header( $bool ) {
		$this->hide_header = ( bool ) $bool;
	}

	/**
	 * Set whether to hide the component footer
	 *
	 * @param boolean $bool
	 */
	public function hide_footer( $bool ) {
		$this->hide_footer = ( bool ) $bool;
	}

	/**
	 * Set whether to hide the component title
	 *
	 * @param boolean $bool
	 */
	public function hide_title( $bool ) {
		$this->hide_title = ( bool ) $bool;
	}

	/**
	 * Set whether to hide the close button in the component header
	 *
	 * @param boolean $bool
	 */
	public function hide_header_close_button( $bool ) {
		$this->hide_header_close_button = ( bool ) $bool;
	}

	public function has_header_hidden() {
		return ( bool ) $this->hide_header;
	}

	public function has_footer_hidden() {
		return ( bool ) $this->hide_footer;
	}

	public function has_title_hidden() {
		return ( bool ) $this->hide_title;
	}

	public function has_header_close_button_hidden() {
		return ( bool ) $this->hide_header_close_button;
	}

	public function get_title() {
		return $this->title;
	}

	public function get_title_class() {
		return array_merge( $this->title_class, [ 'nm-component-title' ] );
	}

	public function get_title_tag() {
		return $this->title_tag;
	}

	public function get_header_attributes( $formatted = false ) {
		$default = array(
			'class' => array(
				'nm-component-header'
			),
		);
		$atts = $this->merge_args( $default, $this->header_attributes );
		return $formatted ? $this->format_attributes( $atts ) : $atts;
	}

	public function get_header() {
		return $this->header;
	}

	public function get_footer() {
		return $this->footer;
	}

	public function get_footer_attributes( $formatted = false ) {
		$default = [
			'class' => [ 'nm-component-footer' ],
		];
		$atts = $this->merge_args( $default, $this->footer_attributes );
		return $formatted ? $this->format_attributes( $atts ) : $atts;
	}

	/**
	 * Get the 'back' button for navigating between components
	 * @param array $args Arguments used to compose the button html. Arguments supplied
	 * 									  are merged with the default arguments. Possible arguments:
	 * - text {string} The text to display in the button. Default "Back"
	 * - attributes {array} Attributes to add to the button such as class and data attributes.
	 * @param string $action The type of result to return. Valid values are
	 * - replace : Whether to replace the default arguments with the supplied arguments instead of merging.
	 * - args: Whether to return only the arguments used to compose the button instead of the button html.
	 * Default value is "null" which is to merge the default arguments with the supplied arguments.
	 * @return mixed
	 */
	public function get_back_button( $args = array(), $action = null ) {
		$attributes = $this->get_default_button_attributes();
		// show_back_button property can be used to set the data target
		$attributes[ 'data-bs-target' ] = $this->show_back_button;
		$attributes[ 'data-bs-toggle' ] = $this->get_type();
		$attributes[ 'data-bs-dismiss' ] = $this->get_type();
		$attributes[ 'class' ][] = 'nm-back';
		$defaults = [ 'text' => __( 'Back', 'nmerimedia' ), 'attributes' => $attributes ];

		return 'args' === $action ? $defaults : $this->compose_button( $defaults, $args, $action );
	}

	/**
	 * Get the 'next' button for navigating between components
	 * @param array $args Arguments used to compose the button html. Arguments supplied
	 * 									  are merged with the default arguments. Possible arguments:
	 * - text {string} The text to display in the button. Default "Next"
	 * - attributes {array} Attributes to add to the button such as class and data attributes.
	 * @param string $action The type of result to return. Valid values are
	 * - replace : Whether to replace the default arguments with the supplied arguments instead of merging.
	 * - args: Whether to return only the arguments used to compose the button instead of the button html.
	 * Default value is "null" which is to merge the default arguments with the supplied arguments.
	 * @return mixed
	 */
	public function get_next_button( $args = array(), $action = null ) {
		$attributes = $this->get_default_button_attributes();
		// show_next_button property can be used to set the data target
		$attributes[ 'data-bs-target' ] = $this->show_next_button;
		$attributes[ 'data-bs-toggle' ] = $this->get_type();
		$attributes[ 'class' ][] = 'nm-next';
		$defaults = [ 'text' => __( 'Next', 'nmerimedia' ), 'attributes' => $attributes ];

		return 'args' === $action ? $defaults : $this->compose_button( $defaults, $args, $action );
	}

	/**
	 * Get the 'cancel' button for closing the component. Typically used in the footer.
	 * @param array $args Arguments used to compose the button html. Arguments supplied
	 * 									  are merged with the default arguments. Possible arguments:
	 * - text {string} The text to display in the button. Default "Cancel"
	 * - attributes {array} Attributes to add to the button such as class and data attributes.
	 * @param string $action The type of result to return. Valid values are
	 * - replace : Whether to replace the default arguments with the supplied arguments instead of merging.
	 * - args: Whether to return only the arguments used to compose the button instead of the button html.
	 * Default value is "null" which is to merge the default arguments with the supplied arguments.
	 * @return mixed
	 */
	public function get_cancel_button( $args = array(), $action = null ) {
		$attributes = $this->get_default_button_attributes();
		$attributes[ 'data-bs-dismiss' ] = $this->get_type();
		$attributes[ 'class' ][] = 'nm-cancel';
		$defaults = [ 'text' => __( 'Cancel', 'nmerimedia' ), 'attributes' => $attributes ];

		return 'args' === $action ? $defaults : $this->compose_button( $defaults, $args, $action );
	}

	/**
	 * Get the 'save' button for saving the component contents. Typically used in the footer.
	 * @param array $args Arguments used to compose the button html. Arguments supplied
	 * 									  are merged with the default arguments. Possible arguments:
	 * - text {string} The text to display in the button. Default "Save".
	 * - attributes {array} Attributes to add to the button such as class and data attributes.
	 * @param string $action The type of result to return. Valid values are
	 * - replace : Whether to replace the default arguments with the supplied arguments instead of merging.
	 * - args: Whether to return only the arguments used to compose the button instead of the button html.
	 * Default value is "null" which is to merge the default arguments with the supplied arguments.
	 * @return mixed
	 */
	public function get_save_button( $args = array(), $action = null ) {
		$attributes = $this->get_default_button_attributes();
		$attributes[ 'class' ][] = 'nm-save';
		$defaults = [ 'text' => __( 'Save', 'nmerimedia' ), 'attributes' => $attributes ];

		return 'args' === $action ? $defaults : $this->compose_button( $defaults, $args, $action );
	}

	public function compose_button( $defaults, $args, $action ) {
		if ( 'replace' === $action ) {
			$params = $args;
		} else {
			$params = $this->merge_button_args( $defaults, $args );
		}

		return '<button ' . $this->format_attributes( $params[ 'attributes' ] ) .
			'>' . esc_html( $params[ 'text' ] ) . '</button>';
	}

	public function get_default_button_attributes() {
		return [
			'type' => 'button',
			'class' => [
				'btn',
				'button',
				'nm-dialog-button',
			],
		];
	}

	public function merge_button_args( $defaults, $args ) {
		$result = $defaults;
		if ( isset( $args[ 'attributes' ] ) ) {
			$result[ 'attributes' ] = $this->merge_args( $defaults[ 'attributes' ], $args[ 'attributes' ] );
			unset( $args[ 'attributes' ] );
		}
		return $this->merge_args( $result, $args );
	}

}
