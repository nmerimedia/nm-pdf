<?php

namespace NmeriMedia\V1\Bootstrap;

defined( 'ABSPATH' ) || exit;

/**
 * @class \NmeriMedia\V1\Bootstrap\Component
 */
class Component {

	use \NmeriMedia\V1\Traits\Utils;

	public $id = '';
	public $type;
	public $content = '';
	public $container_attributes = [];
	public $body_attributes = [];

	public function __construct( $args = array() ) {
		do_action( 'nm_before_component', $this );
		die( 'er' );
		$this->set_args( $args );
	}

	/*
	 * ----------------------------------------------------
	 * SETTERS
	 * ----------------------------------------------------
	 */

	public function set_id( $id ) {
		$this->id = $id;
	}

	public function set_args( $args ) {
		if ( !empty( $args ) ) {
			foreach ( get_object_vars( $this ) as $key => $val ) {
				if ( array_key_exists( $key, $args ) ) {
					$value = $args[ $key ];
					if ( is_callable( array( $this, "set_$key" ) ) ) {
						$this->{"set_$key"}( $value );
					} else {
						$this->{$key} = $value;
					}
				}
			}
		}
	}

	public function set_type( $type ) {
		$this->type = $type;
	}

	public function set_content( $content ) {
		$this->content = $content;
	}

	public function set_attributes( $key, $attributes, $replace = false ) {
		$prop = $key . '_attributes';
		if ( $replace ) {
			$this->{$prop} = $attributes;
		} else {
			$this->{$prop} = array_merge_recursive( $this->{$prop}, $attributes );
		}
	}

	public function set_container_attributes( $attributes, $replace = false ) {
		$this->set_attributes( 'container', $attributes, $replace );
	}

	public function set_body_attributes( $attributes, $replace = false ) {
		$this->set_attributes( 'body', $attributes, $replace );
	}

	/*
	 * ----------------------------------------------------
	 * GETTERS
	 * ----------------------------------------------------
	 */

	public function get_type() {
		return $this->type;
	}

	public function get_id() {
		return $this->id;
	}

	public function get_content() {
		return $this->content;
	}

	/**
	 * Alias of get_content()
	 */
	public function get_body() {
		return $this->get_content();
	}

	public function get_container_attributes( $formatted = false ) {
		$default = array(
			'class' => array(
				'nm-component',
				'nm'
			),
			'id' => $this->get_id()
		);
		$atts = $this->merge_args( $default, $this->container_attributes );
		return $formatted ? $this->format_attributes( $atts ) : $atts;
	}

	public function get_body_attributes( $formatted = false ) {
		$default = array(
			'class' => array(
				'nm-component-body'
			),
		);
		$atts = $this->merge_args( $default, $this->body_attributes );
		return $formatted ? $this->format_attributes( $atts ) : $atts;
	}

	public function get_comp_template() {
		return \NmeriMedia\V1\Utils::path() . 'templates/' . $this->get_type() . '.php';
	}

	/*
	 * ----------------------------------------------------
	 * OTHER METHODS
	 * ----------------------------------------------------
	 */

	/**
	 * Get the component
	 * @return string
	 */
	public function get() {
		ob_start();
		$this->print();
		$template = ob_get_clean();
		return $template;
	}

	/**
	 * Output the component html
	 */
	public function print() {
		include $this->get_comp_template();
		do_action( 'nm_after_component', $this );
	}

	public function add_class( $key, $class ) {
		$prop = $key . '_attributes';
		$arr = is_array( $class ) ? $class : array( $class );
		$this->{$prop}[ 'class' ] = array_merge( $this->{$prop}[ 'class' ], $arr );
	}

	/**
	 * Add extra classes to the component container
	 * @param array|string $class Classes to add
	 */
	public function add_container_class( $class ) {
		$this->set_container_attributes( [ 'class' => $class ] );
	}

}
