<?php

namespace NmeriMedia\V1\Bootstrap;

defined( 'ABSPATH' ) || exit;

/**
 * @class \NmeriMedia\V1\Bootstrap\Offcanvas
 */
class Offcanvas extends Dialog {

	public $type = 'offcanvas';
	public $container_attributes = array(
		'class' => array(
			'offcanvas',
			'offcanvas-end',
		),
	);
	public $header_attributes = array(
		'class' => array(
			'offcanvas-header'
		),
	);
	public $footer_attributes = array(
		'class' => array(
			'offcanvas-footer'
		),
	);
	public $body_attributes = array(
		'class' => array(
			'offcanvas-body'
		),
	);
	public $title_class = [ 'offcanvas-title' ];

}
