<?php

namespace NmeriMedia\V1\Bootstrap;

defined( 'ABSPATH' ) || exit;

/**
 * @class \NmeriMedia\V1\Bootstrap\Popover
 */
class Popover extends Component {

	public $type = 'popover';
	public $text = '';
	public $tag = 'a';
	public $container_attributes = array(
		'class' => array(
			'button',
		),
		'data-bs-toggle' => 'popover',
		'role' => 'button',
		'id' => '',
	);

	/**
	 * Set the tag to be used for the popover button
	 * @param string $tag Default is 'a'
	 * @return $this
	 */
	public function set_tag( $tag ) {
		$this->tag = $tag;
		return $this;
	}

	/**
	 * Get the tag to be used for the popover button
	 * @return string
	 */
	public function get_tag() {
		return $this->tag;
	}

	/**
	 * Set the direction of the popover
	 * The default direction is "right".
	 * @param string $direction The direction Values are top, bottom, right, left.
	 * @return $this
	 */
	public function set_direction( $direction ) {
		$this->container_attributes[ 'data-bs-placement' ] = $direction;
		return $this;
	}

	/**
	 * Set the button text for the popover
	 * @param string $text
	 * @return $this
	 */
	public function set_text( $text ) {
		$this->text = $text;
		return $this;
	}

	/**
	 * Set the content of the popover
	 * @param string $content
	 * @return $this
	 */
	public function set_content( $content ) {
		$this->container_attributes[ 'data-bs-content' ] = $content;
		return $this;
	}

	public function set_title( $title ) {
		$this->container_attributes[ 'title' ] = $title;
		$this->container_attributes[ 'data-bs-title' ] = $title;
		return $this;
	}

	/**
	 * Dismiss the popover on click
	 * @return $this
	 */
	public function dismiss_on_click() {
		$this->container_attributes[ 'data-bs-trigger' ] = 'focus';
		$this->set_tag( 'a' );
		return $this;
	}

	/**
	 * Dismiss the popover on hover
	 * @return $this
	 */
	public function dismiss_on_hover() {
		$this->container_attributes[ 'data-bs-trigger' ] = 'hover';
		return $this;
	}

	/**
	 * Dismiss the popover on hover and click
	 * @return $this
	 */
	public function dismiss_on_hover_and_click() {
		$this->container_attributes[ 'data-bs-trigger' ] = 'hover focus';
		$this->set_tag( 'a' );
		return $this;
	}

	/**
	 * Get the button text for the popover
	 * @return string
	 */
	public function get_text() {
		return $this->text;
	}

	public function get_container_attributes( $formatted = false ) {
		if ( 'a' === $this->get_tag() && !isset( $this->container_attributes[ 'tabindex' ] ) ) {
			$this->container_attributes[ 'tabindex' ] = '0';
		}
		return parent::get_container_attributes( $formatted );
	}

}
