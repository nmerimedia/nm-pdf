<?php

namespace NmeriMedia\V1\Bootstrap;

defined( 'ABSPATH' ) || exit;

/**
 * @class \NmeriMedia\V1\Bootstrap\Dropdown
 */
class Dropdown extends Component {

	public $type = 'dropdown';
	public $menu_direction = '';
	public $menu_items = [];
	public $menu_content;
	public $is_split = false;
	public $container_attributes = array(
		'class' => array(
			'dropdown',
		),
	);
	public $text = '';
	public $toggle_attributes = array(
		'class' => array(
			'dropdown-toggle',
		),
		'data-bs-toggle' => 'dropdown',
		'aria-expanded' => 'false',
		'role' => 'button',
		'id' => '',
	);
	public $menu_attributes = array(
		'class' => array(
			'dropdown-menu'
		),
	);

	/**
	 * Set the content of the dropdown menu
	 * This should be used if not setting menu items
	 * @param $content String
	 * return $this
	 */
	public function set_menu_content( $content ) {
		$this->menu_content = $content;
		return $this;
	}

	/**
	 * Get the content of the dropdown menu
	 * @return string
	 */
	public function get_menu_content() {
		return $this->menu_content;
	}

	/**
	 * Split the drodown so that the button is separate from the toggle
	 * @return $this
	 */
	public function split() {
		$this->is_split = true;
		$this->toggle_attributes[ 'class' ][] = 'dropdown-toggle-split';
		$this->container_attributes[ 'class' ][] = 'btn-group';
		return $this;
	}

	/**
	 * Check if the dropdown is split
	 * @return boolean
	 */
	public function is_split() {
		return $this->is_split;
	}

	public function hide_arrow() {
		$key = array_search( 'dropdown-toggle', ($this->toggle_attributes[ 'class' ] ?? [] ) );
		if ( is_int( $key ) ) {
			unset( $this->toggle_attributes[ 'class' ][ $key ] );
		}
	}

	/**
	 * Set the direction where the dropdown menu should appear
	 * The default direction is "down".
	 * @param string $direction The direction Values are up, right, left.
	 * @return $this
	 */
	public function set_menu_direction( $direction ) {
		$this->menu_direction = $direction;
		$this->set_menu_direction_class( $direction );
		return $this;
	}

	/**
	 * Set the class of the direction of the dropdown menu
	 * @param string $direction The direction. Values are up, right, left, down. Default is down
	 */
	public function set_menu_direction_class( $direction ) {
		switch ( $direction ) {
			case 'up':
				$this->container_attributes[ 'class' ][] = 'dropup';
				break;
			case 'right':
				$this->container_attributes[ 'class' ][] = 'dropend';
				break;
			case 'left':
				$this->container_attributes[ 'class' ][] = 'dropstart';
				break;
		}
		return $this;
	}

	/**
	 * Set the button text for the dropdown
	 * @param string $text
	 * @return $this
	 */
	public function set_text( $text ) {
		$this->text = $text;
		return $this;
	}

	/**
	 * Add a dropdown menu item
	 * @param string $text The text of the item
	 * @param array $attributes Html attributes for the menu item element
	 * @param string|int $key The key used to identify the menu item in the menu item array
	 * @param string $tag The element tag to use for the menu item
	 * @return $this
	 */
	public function add_menu_item( $text, $attributes, $key, $tag ) {
		$item = [
			'tag' => $tag,
			'text' => $text,
			'attributes' => $attributes
		];

		if ( $key ) {
			$this->menu_items[ $key ] = $item;
		} else {
			$this->menu_items[] = $item;
		}

		return $this;
	}

	/**
	 * Add a normal menu item to the dropdown menu
	 * @param string $text The text of the item
	 * @param array $attributes Html attributes for the menu item element
	 * @param string $key String used to identify the menu item in the menu item array
	 * @param string $tag Html tag to use for the menu item
	 * @return $this
	 */
	public function set_menu_item( string $text, array $attributes = [], $key = '', $tag = 'a' ) {
		$attr = \NmeriMedia\V1\Utils::merge_args( [ 'class' => [ 'dropdown-item' ] ], $attributes );
		$attr[ 'style' ] = ($attr[ 'style' ] ?? '') . 'width:auto;'; // adjust width to fix bug in wp admin area
		return $this->add_menu_item( $text, $attr, $key, $tag );
	}

	/**
	 * Add a text menu item to the dropdown menu.
	 * This is simply a text that doesn't link to anywhere
	 *
	 * @param string $text The text of the item
	 * @param array $attributes Html attributes for the menu item element
	 * @param string $tag Html tag to use for the menu item
	 * @return $this
	 */
	public function set_menu_text( string $text, array $attributes = [], $key = '', $tag = 'span' ) {
		$attr = \NmeriMedia\V1\Utils::merge_args( [ 'class' => [ 'dropdown-item-text' ] ], $attributes );
		return $this->add_menu_item( $text, $attr, $key, $tag );
	}

	/**
	 * Add a header to the dropdown menu
	 * @param string $text The text of the header
	 * @param array $attributes Html attributes for the header element
	 * @param string $tag Html tag to use for the header
	 * @return $this
	 */
	public function set_menu_header( string $text, array $attributes = [], $key = '', $tag = 'h6' ) {
		$attr = \NmeriMedia\V1\Utils::merge_args( [ 'class' => [ 'dropdown-header' ] ], $attributes );
		$attr[ 'style' ] = ($attr[ 'style' ] ?? '') . 'margin:0;'; // add margin to fix bug in wp admin area
		return $this->add_menu_item( $text, $attr, $key, $tag );
	}

	/**
	 * Add a divider to separate the dropdown menu items
	 * @param array $attributes Html attributes for the divider element
	 * @param string $tag Html tag to use for the divider
	 * @return $this
	 */
	public function set_menu_divider( $attributes = [], $key = '', $tag = 'hr' ) {
		$attr = \NmeriMedia\V1\Utils::merge_args( [ 'class' => [ 'dropdown-divider' ] ], $attributes );
		return $this->add_menu_item( '', $attr, $key, $tag );
	}

	/**
	 * Get all the menu items
	 * @return array
	 */
	public function get_menu_items() {
		return $this->menu_items;
	}

	/**
	 * Get the html element for a menu item
	 * @param array $item The menu item argument
	 * @return string
	 */
	public function get_menu_item_html( $item ) {
		$text = $item[ 'text' ] ?? null;
		$attr = isset( $item[ 'attributes' ] ) ? \NmeriMedia\V1\Utils::format_attributes( $item[ 'attributes' ] ) : null;
		$opening_tag = "<{$item[ 'tag' ]} ";
		$closing_tag = $text ? " </{$item[ 'tag' ]}>" : null;
		return $opening_tag . $attr . '>' . $text . $closing_tag;
	}

	/**
	 * Get the attributes for the dropdown toggle element
	 * @param boolean $formatted Whether the attributes should be formatted. Default true.
	 * @return mixed
	 */
	public function get_toggle_attributes( $formatted = true ) {
		return $formatted ? $this->format_attributes( $this->toggle_attributes ) : $this->toggle_attributes;
	}

	/**
	 * Get the attributes for the dropdown menu element
	 * @param boolean $formatted Whether the attributes should be formatted. Default true.
	 * @return mixed
	 */
	public function get_menu_attributes( $formatted = true ) {
		return $formatted ? $this->format_attributes( $this->menu_attributes ) : $this->menu_attributes;
	}

	/**
	 * Get the button text for the dropdown
	 * @return string
	 */
	public function get_text() {
		return $this->text;
	}

	public function has_items() {
		return !empty( $this->get_menu_items() );
	}

}
