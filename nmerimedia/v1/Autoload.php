<?php

namespace NmeriMedia\V1;

defined( 'ABSPATH' ) || exit;

/**
 * @class \NmeriMedia\V1\Autoload
 */
class Autoload {

	public static $namespaces = [
		'NmeriMedia\V1' => __DIR__,
	];

	public static function add_namespace( $namespace, $path ) {
		self::$namespaces[ $namespace ] = $path;
	}

	public static function load( $class ) {
		foreach ( self::$namespaces as $namespace => $dir ) {
			if ( false === strpos( $class, $namespace ) ) {
				continue;
			}

			// Replace the namespace with the directory
			$path1 = str_replace( $namespace, trailingslashit( $dir ), $class );
			// Change the namespace separators to directory separators
			$path2 = str_replace( '\\', '/', $path1 );
			// Add the file extension
			$path = $path2 . '.php';

			if ( file_exists( $path ) ) {
				include $path;
			}
		}
	}

	public static function run() {
		spl_autoload_register( array( __CLASS__, 'load' ) );
	}

}
