<?php

namespace NmeriMedia\V1;

use NmeriMedia\V1\Utils as NM_Utils;

defined( 'ABSPATH' ) || exit;

/**
 * @class \NmeriMedia\V1\Traits\Scripts
 */
class Scripts {

	public function get_url( $path = '' ) {
		$url = NM_Utils::url();
		return $path ? $url . $path : $url;
	}

	public function get_version() {
		return NM_Utils::version();
	}

	public function get_prefix() {
		return 'nmerimedia-v1-';
	}

	public function get_prefixed_handle( $handle ) {
		return $this->get_prefix() . $handle;
	}

	public function get_styles() {
		$styles = [
			'select2' => [
				'src' => $this->get_url( 'assets/css/select2.min.css' ),
				'deps' => [],
				'version' => $this->get_version(),
			],
			'bootstrap' => [
				'src' => $this->get_url( 'assets/css/bootstrap.min.css' ),
				'deps' => [],
				'version' => '5.0.2',
			],
			'admin' => [
				'src' => $this->get_url( 'assets/css/admin.min.css' ),
				'deps' => [],
				'version' => $this->get_version(),
			],
			'frontend' => [
				'src' => $this->get_url( 'assets/css/frontend.min.css' ),
				'deps' => [],
				'version' => $this->get_version(),
			],
		];
		return $styles;
	}

	public function get_scripts() {
		$scripts = [
			'select2' => [
				'src' => $this->get_url( 'assets/js/select2.full.min.js' ),
				'deps' => [ 'jquery' ],
				'version' => '4.0.3',
				'in_footer' => true,
			],
			'bootstrap' => [
				'src' => $this->get_url( 'assets/js/bootstrap.min.js' ),
				'deps' => [ 'jquery' ],
				'version' => '5.0.2',
				'in_footer' => true,
			],
			'admin' => [
				'src' => $this->get_url( 'assets/js/admin.min.js' ),
				'deps' => [ 'jquery' ],
				'version' => $this->get_version(),
				'in_footer' => true,
			],
			'popper' => [
				'src' => $this->get_url( 'assets/js/popper.min.js' ),
				'deps' => [],
				'version' => '2.9.2',
				'in_footer' => true,
			],
		];
		return $scripts;
	}

	public function get_style( $handle ) {
		return $this->get_styles()[ $handle ] ?? null;
	}

	public function get_script( $handle ) {
		return $this->get_scripts()[ $handle ] ?? null;
	}

	public function enqueue_style( $handle ) {
		$style = $this->get_style( $handle );
		if ( $style ) {
			wp_enqueue_style(
				$this->get_prefixed_handle( $handle ),
				$style[ 'src' ] ?? '',
				$style[ 'deps' ] ?? [],
				$style[ 'version' ] ?? false
			);
		}
	}

	public function register_style( $handle ) {
		$style = $this->get_style( $handle );
		if ( $style ) {
			wp_register_style(
				$this->get_prefixed_handle( $handle ),
				$style[ 'src' ] ?? false,
				$style[ 'deps' ] ?? [],
				$style[ 'version' ] ?? false
			);
		}
	}

	public function enqueue_script( $handle ) {
		$script = $this->get_script( $handle );
		if ( $script ) {
			wp_enqueue_script(
				$this->get_prefixed_handle( $handle ),
				$script[ 'src' ] ?? '',
				$script[ 'deps' ] ?? [],
				$script[ 'version' ] ?? false,
				$script[ 'in_footer' ] ?? true
			);
		}
	}

	public function register_script( $handle ) {
		$script = $this->get_script( $handle );
		if ( $script ) {
			wp_register_script(
				$this->get_prefixed_handle( $handle ),
				$script[ 'src' ] ?? false,
				$script[ 'deps' ] ?? [],
				$script[ 'version' ] ?? false,
				$script[ 'in_footer' ] ?? true
			);
		}
	}

}
