<?php

namespace NmeriMedia\V1;

use \NmeriMedia\V1\Licenses,
		\NmeriMedia\V1\Scripts;

defined( 'ABSPATH' ) || exit;

/**
 * @class \NmeriMedia\V1\Settings
 */
abstract class Settings {

	use \NmeriMedia\V1\Traits\Utils,
		 \NmeriMedia\V1\Traits\PluginFn;

	/**
	 * The current tab we are on
	 * @var string
	 */
	public $current_tab;

	/**
	 * The current section we are on
	 * @var string
	 */
	public $current_section;

	/**
	 * Name of options in the database table
	 * (also used as the options_group value in the 'register_setting' function)
	 * @var string
	 */
	public $option_name;

	/**
	 * Whether this page is a woocommerce screen, so that we can enqueue woocommerce scripts
	 * @var boolean
	 */
	public $is_woocommerce_screen = false;

	/**
	 * Whether this page is an nmerimedia screen, so that we can enqueue nmerimedia scripts
	 * @var boolean
	 */
	public $is_nmerimedia_screen = true;

	/**
	 * Set up settings menu and page for this plugin
	 * @param array $plugin_fn_name The name of the main plugin function.
	 * 															This allows us to have access to the main plugin instance,
	 * 															properties, and methods
	 */
	public function __construct( $plugin_fn_name = null ) {
		$this->plugin_fn_name = $plugin_fn_name;
		if ( !$this->option_name && !empty( $this->plugin_fn()->base ) ) {
			$this->option_name = $this->plugin_fn()->base . '_settings';
		}
	}

	public function init() {
		add_filter( 'woocommerce_screen_ids', array( $this, 'add_woocommerce_screen_id' ) );
		add_action( 'admin_menu', array( $this, 'add_menu_page' ) );
		add_action( 'admin_footer', [ $this, 'enqueue_sprite_file' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_style' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_script' ] );

		if ( $this->option_name ) {
			add_filter( 'pre_update_option_' . $this->option_name, array( $this, 'pre_update_option' ), 10, 2 );
			add_action( 'update_option_' . $this->option_name, array( $this, 'update_option' ), 10, 2 );
		}

		add_action( 'admin_init', array( $this, 'add_settings_fields' ) );
		add_action( 'admin_menu', array( $this, 'show_saved_settings_errors' ) );
		add_action( 'wp_ajax_nmerimedia_search_products', array( $this, 'search_products' ) );

		if ( $this->page_slug() === filter_input( INPUT_GET, 'page' ) ) {
			$this->current_tab = $this->get_current_tab();
			$this->current_section = $this->get_current_section();
		}

		if ( !is_null( $this->plugin_fn() ) && $this->plugin_fn()->is_licensed && $this->plugin_fn()->basename ) {
			$this->init_license_module();
		}
	}

	/**
	 * Add the screen id of the current plugin settings page to the
	 * array of woocommerce screen ids
	 */
	public function add_woocommerce_screen_id( $screen_ids ) {
		if ( $this->is_woocommerce_screen && $this->is_current_screen() ) {
			$screen_ids[] = $this->get_screen_id();
		}
		return $screen_ids;
	}

	/**
	 * Get the screen id of the current plugin settings page
	 * @return string
	 */
	public function get_screen_id() {
		return !is_null( $this->plugin_fn() ) ? 'nmeri-media_page_' . $this->plugin_fn()->slug : '';
	}

	/**
	 * Check if the current admin screen being viewed is an nmerimedia screen
	 * @return boolean
	 */
	public function is_nmerimedia_screen() {
		return $this->is_nmerimedia_screen && $this->is_current_screen();
	}

	/**
	 * Check if the current screen being viewed is for this settings page
	 * @return boolean
	 */
	public function is_current_screen() {
		return $this->get_current_screen_id() === $this->get_screen_id();
	}

	private function init_license_module() {
		add_filter( 'nmerimedia_packages', array( $this, 'add_license_package' ) );
		Licenses::get_instance();
	}

	public function add_license_package( $packages ) {
		if ( $this->plugin_fn()->basename ) {
			$packages[] = $this->plugin_fn()->basename;
		}
		return $packages;
	}

	public function enqueue_sprite_file() {
		if ( $this->is_nmerimedia_screen() ) {
			$this->include_base_sprite_file();
		}
	}

	public function enqueue_style() {
		if ( $this->is_nmerimedia_screen() ) {
			$script = new Scripts();
			$script->enqueue_style( 'bootstrap' );
			$script->enqueue_style( 'admin' );
		}
	}

	public function enqueue_script() {
		if ( $this->is_nmerimedia_screen() ) {
			$script = new Scripts();
			$script->enqueue_script( 'popper' );
			$script->enqueue_script( 'bootstrap' );
			$script->enqueue_script( 'admin' );
		}
	}

	/**
	 * Parameters to use with add_menu_page for the parent menu
	 * if the settings page would be a submenu
	 *
	 * @return array
	 */
	public function parent_menu_params() {
		$title = __( 'Nmeri Media', 'nmerimedia' );
		$icon = base64_encode( $this->get_svg( [
				'icon' => 'nmerimedia-icon',
				'sprite' => false,
			] ) );

		return array(
			'page_title' => $title,
			'menu_title' => $title,
			'capability' => 'manage_options',
			'menu_slug' => 'nmerimedia',
			'function' => array( $this, 'menu_page_content' ),
			'icon_url' => 'data:image/svg+xml;base64,' . $icon,
			'position' => null
		);
	}

	/**
	 * Slug of settings page
	 * @return string
	 */
	public function page_slug() {
		return $this->plugin_fn()->slug ?? null;
	}

	/**
	 * Get the url of the plugin settings page
	 * @return string
	 */
	public function get_page_url() {
		$slug = $this->page_slug();
		return $slug ? menu_page_url( $slug, false ) : '';
	}

	/**
	 * The menu page configuration.
	 *
	 * This function must return an array contain the parameters for either the wordpress function
	 * 'add_menu_page' or 'add_submenu_page' for the menu page to show up in the admin menu.
	 *
	 * Parameters accepted (as array keys):
	 *
	 * - parent_slug {optional|required} Required only if the page would be a submenu page.
	 * - page_title {required}
	 * - menu_title {required}
	 * - capability {required}
	 * - icon_url - {optional} Only used for top level menu pages
	 * - position - {optional}
	 *
	 * The other parameters typically used in the "add_menu_page" or "add_submenu_page" functions
	 * such as the 'menu_slug' and the callback function for displaying the page content are taken
	 * from the class methods such as 'page_slug()' and 'menu_page_content()' respectively.
	 *
	 * @return array
	 *
	 */
	protected function menu_params() {
		$title = $this->plugin_fn()->name;
		return array(
			'parent_slug' => $this->parent_menu_params()[ 'menu_slug' ],
			'page_title' => $title,
			'menu_title' => 0 === strpos( $title, 'NM ' ) ? str_replace( 'NM ', '', $title ) : $title,
			'capability' => 'manage_options',
			'position' => 1
		);
	}

	/**
	 * Get all the settings tabs registered for the plugin
	 *
	 * This returns an array of arrays where each array key represents the slug for the
	 * settings tab and the array value is an array containing keys:
	 * - tab_title {string}  The title of the tab
	 * - sections_title {string} The title to show for all the tab sections
	 * - show_sections {boolean} whether to show the sections all at once (using do_settings_sections)
	 * - sections {array} Sections to show on the tab where each array key represents the section slug and
	 * 		                the array value represents the section title.
	 *
	 * @return array
	 */
	protected function get_tabs() {
		return array();
	}

	/**
	 * Actions to perform before an option is updated.
	 * Use this only if you have set 'option_name' and you are saving options.
	 */
	public function pre_update_option( $new_value, $old_value ) {
		return $new_value;
	}

	/**
	 * Actions to perform before an option is updated.
	 * Use this only if you have set 'option_name' and you are saving options.
	 */
	public function update_option( $old_value, $new_value ) {

	}

	/**
	 * Get the saved settings option from the database
	 *
	 * @param string {optional} The field key to get from the options array
	 * @param mixed  {optional} The value to set for the field key if it doesn't exist
	 * @return mixed The field key value or the entire option value if no field key is specified.
	 */
	public function get_option( $field_key = '', $default_value = null ) {
		$option = get_option( $this->option_name, array() );
		$options = is_array( $option ) ? $option : (!$option ? array() : array( $option ));
		if ( $field_key ) {
			return array_key_exists( $field_key, $options ) ? $options[ $field_key ] : $default_value;
		}
		return $options;
	}

	public function add_menu_page() {
		if ( !empty( $this->menu_params() ) && array_key_exists( 'parent_slug', $this->menu_params() ) ) {
			if ( $this->parent_menu_params()[ 'menu_slug' ] === $this->menu_params()[ 'parent_slug' ] &&
				!menu_page_url( $this->parent_menu_params()[ 'menu_slug' ], false ) ) {
				add_menu_page(
					$this->parent_menu_params()[ 'page_title' ],
					$this->parent_menu_params()[ 'menu_title' ],
					$this->parent_menu_params()[ 'capability' ],
					$this->parent_menu_params()[ 'menu_slug' ],
					$this->parent_menu_params()[ 'function' ],
					$this->parent_menu_params()[ 'icon_url' ],
					$this->parent_menu_params()[ 'position' ]
				);
			}

			$position = null;
			if ( isset( $this->menu_params()[ 'position' ] ) ) {
				$position = $this->menu_params()[ 'position' ];
			} elseif ( $this->parent_menu_params()[ 'menu_slug' ] === $this->menu_params()[ 'parent_slug' ] ) {
				$position = 0;
			}

			add_submenu_page(
				$this->menu_params()[ 'parent_slug' ],
				$this->menu_params()[ 'page_title' ],
				$this->menu_params()[ 'menu_title' ],
				$this->menu_params()[ 'capability' ],
				$this->page_slug(),
				array( $this, 'menu_page_content' ),
				$position
			);

			if ( $this->parent_menu_params()[ 'menu_slug' ] === $this->menu_params()[ 'parent_slug' ] &&
				menu_page_url( $this->parent_menu_params()[ 'menu_slug' ], false ) ) {
				remove_submenu_page( $this->parent_menu_params()[ 'menu_slug' ], $this->parent_menu_params()[ 'menu_slug' ] );
			}
		} elseif ( !empty( $this->menu_params() ) ) {
			add_menu_page(
				$this->menu_params()[ 'page_title' ],
				$this->menu_params()[ 'menu_title' ],
				$this->menu_params()[ 'capability' ],
				$this->page_slug(),
				array( $this, 'menu_page_content' ),
				isset( $this->menu_params()[ 'icon_url' ] ) ? $this->menu_params()[ 'icon_url' ] : null,
				isset( $this->menu_params()[ 'position' ] ) ? $this->menu_params()[ 'position' ] : null
			);
		}
	}

	/**
	 * The key used to save the settings errors in the database
	 * @return string.
	 */
	public function get_settings_errors_key() {
		if ( $this->option_name ) {
			return $this->option_name . '_errors';
		}
	}

	/**
	 * Outputs the template (tabs, tab sections) for the menu page content
	 * Override this if you want to set a custom menu page content
	 */
	public function menu_page_content() {
		?>
		<div class="wrap nmerimedia-settings <?php echo esc_attr( $this->page_slug() ) . ' ' . esc_attr( $this->current_tab ); ?>">
			<form method="post" action="options.php" enctype="multipart/form-data">
				<div class="container">
					<div class="main">
						<?php if ( 1 < count( $this->get_tabs() ) ) : ?>
							<nav class="nav-tab-wrapper">
								<?php
								foreach ( $this->get_tabs() as $slug => $args ) {
									$tab_title = isset( $args[ 'tab_title' ] ) ? $args[ 'tab_title' ] : $slug;
									$tab_url = add_query_arg( array(
										'page' => $this->page_slug(),
										'tab' => esc_attr( $slug )
										),
										remove_query_arg( 'page', $this->get_page_url() )
									);

									echo '<a href="' . esc_url( $tab_url ) . '" class="nav-tab ' .
									( $this->current_tab === $slug ? 'nav-tab-active ' : '' ) . esc_attr( $slug ) . '">' .
									wp_kses( apply_filters( $this->page_slug() . '_tab_title', $tab_title, $slug, $this ),
										$this->allowed_post_tags() ) .
									'</a>';
								}
								?>
							</nav>
						<?php endif; ?>

						<?php
						$args = isset( $this->get_tabs()[ $this->current_tab ] ) ? $this->get_tabs()[ $this->current_tab ] : '';

						$sections_title = isset( $args[ 'sections_title' ] ) ? $args[ 'sections_title' ] : null;

						// hack to keep settings_errors() above section titles and section submenu
						printf( '<h1 style=%s>%s</h1>',
							empty( $sections_title ) ? 'display:none;' : '',
							esc_html( $sections_title )
						);

						$current_tab_sections = isset( $this->get_tabs()[ $this->current_tab ][ 'sections' ] ) ? $this->get_tabs()[ $this->current_tab ][ 'sections' ] : array();

						if ( 1 < count( $current_tab_sections ) ) :
							?>
							<ul class="subsubsub">

								<?php
								$section_keys = array_keys( $current_tab_sections );

								foreach ( $current_tab_sections as $key => $label ) {
									$section_url = add_query_arg( array(
										'page' => $this->page_slug(),
										'tab' => esc_attr( $this->current_tab ),
										'section' => sanitize_title( $key )
										),
										remove_query_arg( [ 'page', 'tab', 'section' ], $this->get_page_url() )
									);
									$label = apply_filters( $this->page_slug() . '_tab_section_title', $label, $key, $this );
									echo '<li><a href="' . esc_url( $section_url ) . '" class="' . ( $this->current_section == $key ? 'current' : '' ) . '">' . wp_kses( $label, $this->allowed_post_tags() ) . '</a> ' . ( end( $section_keys ) == $key ? '' : '|' ) . ' </li>';
								}
								?>

							</ul><br class="clear" />
							<?php
						endif;

						settings_errors();
						settings_fields( $this->option_name );

						do_action( $this->page_slug() . '_tabs_after_nav', $this );

						if ( isset( $args[ 'show_sections' ] ) && $args[ 'show_sections' ] ) {
							$key = !empty( $this->current_section ) ? $this->current_section : $this->current_tab;
							do_settings_sections( $key );
						}

						do_action( $this->page_slug() . '_tab_' . $this->current_tab );

						if ( ($this->get_current_tab() && !empty( $this->get_tabs() ) && !isset( $args[ 'submit_button' ] )) ||
							(isset( $args[ 'submit_button' ] ) && $args[ 'submit_button' ]) ) {
							submit_button();
						}
						?>
					</div><!--- .main -->
					<?php
					$sidebar = $this->get_sidebar();
					if ( !empty( $sidebar ) ) :
						?>
						<div class="sidebar">
							<?php echo wp_kses( $sidebar, $this->allowed_post_tags() ); ?>
						</div>
					<?php endif; ?>
				</div><!-- .container --->
			</form>
		</div>
		<?php
	}

	public function get_sidebar_links() {
		$links = [
			'docs' => $this->plugin_fn()->docs_url,
			'review' => $this->plugin_fn()->review_url,
			'support' => $this->plugin_fn()->support_url,
			'product' => $this->plugin_fn()->is_pro ? '' : $this->plugin_fn()->product_url,
		];

		$links_html = '';

		foreach ( $links as $key => $value ) {
			if ( $value ) {
				switch ( $key ) {
					case 'docs':
						$text = __( 'Docs', 'nmerimedia' );
						break;
					case 'review':
						$text = __( 'Review', 'nmerimedia' );
						break;
					case 'support':
						$text = __( 'Support', 'nmerimedia' );
						break;
					case 'product':
						$text = __( 'Get PRO', 'nmerimedia' );
						break;
				}
				$links_html .= '<li><a href="' . $value . '">' . $text . '</a></li>';
			}
		}

		return !empty( $links_html ) ? "<ul>$links_html</ul>" : '';
	}

	/**
	 *
	 * @return string
	 */
	public function get_onsale_plugins() {
		$on_sale_plugins = include Utils::path() . 'templates/plugins.php';
		if ( isset( $on_sale_plugins[ $this->page_slug() ] ) ) {
			unset( $on_sale_plugins[ $this->page_slug() ] );
		}

		if ( !empty( $on_sale_plugins ) ) {
			ob_start();
			include Utils::path() . 'templates/plugins-list.php';
			return ob_get_clean();
		}
	}

	public function get_sidebar_contents() {
		return $this->get_sidebar_links() . $this->get_onsale_plugins();
	}

	public function get_sidebar() {
		ob_start();
		do_action( $this->page_slug() . '_settings_page_before_sidebar' );
		echo wp_kses( $this->get_sidebar_contents(), $this->allowed_post_tags() );
		do_action( $this->page_slug() . '_settings_page_after_sidebar' );
		return ob_get_clean();
	}

	/**
	 * Get the current settings tab being viewed
	 *
	 * @param array $request The associative array used to determine the tab, typically $_GET or HTTP_REFERER
	 * @return string
	 */
	public function get_current_tab( $request = array() ) {
		$page = $request[ 'page' ] ?? sanitize_text_field( wp_unslash( filter_input( INPUT_GET, 'page' ) ) );
		if ( $page && $this->page_slug() === $page ) {
			$tab = $request[ 'tab' ] ?? filter_input( INPUT_GET, 'tab' );
			if ( !empty( $tab ) ) {
				return sanitize_title( wp_unslash( $tab ) );
			} else {
				$tabs = array_keys( $this->get_tabs() );
				return reset( $tabs );
			}
		}
	}

	/**
	 * Get the current settings section being viewed
	 *
	 * @param array $request The associative array used to determine the section, typically
	 * $_GET or HTTP_REFERER
	 * @return string
	 */
	public function get_current_section( $request = array() ) {
		$page = $request[ 'page' ] ?? sanitize_text_field( wp_unslash( filter_input( INPUT_GET, 'page' ) ) );
		if ( $page && $this->page_slug() === $page ) {
			$section = sanitize_title( wp_unslash( $request[ 'section' ] ?? filter_input( INPUT_GET, 'section' ) ) );
			$tab = $this->get_current_tab( $request );

			if ( $tab && !$section ) {
				$tab_args = isset( $this->get_tabs()[ $tab ] ) ? $this->get_tabs()[ $tab ] : '';
				if ( !empty( $tab_args ) && isset( $tab_args[ 'sections' ] ) ) {
					$sec = array_flip( $tab_args[ 'sections' ] );
					$section = reset( $sec );
				}
			}
			return $section;
		}
	}

	/**
	 * Get the default values for all plugin options
	 *
	 * @return array
	 */
	public function get_default_field_values() {
		$fields = array();

		foreach ( array_keys( $this->get_tabs() ) as $tab ) {
			$this_section = $this->get_tab_sections( $tab );
			if ( $this_section ) {
				foreach ( $this_section as $args ) {
					foreach ( $args as $args_key => $args_value ) {
						if ( 'fields' == $args_key ) {
							foreach ( $args_value as $value ) {
// Key to use to save the value
								$option_key = isset( $value[ 'option_name' ] ) ? $value[ 'option_name' ] : (isset( $value[ 'id' ] ) ? $value[ 'id' ] : '');
								if ( $option_key ) {
									if ( isset( $value[ 'option_group' ] ) && $value[ 'option_group' ] ) {
										$fields[ $option_key ][] = isset( $value[ 'default' ] ) ? $value[ 'default' ] : '';
									} else {
										$fields[ $option_key ] = isset( $value[ 'default' ] ) ? $value[ 'default' ] : '';
									}

									if ( is_array( $fields[ $option_key ] ) ) {
										$fields[ $option_key ] = array_filter( $fields[ $option_key ] );
									}
								}
							}
						}
					}
				}
			}
		}

		return $fields;
	}

	/**
	 * Save the default values for all plugin options in the database
	 * (This function should typically only be called on plugin installation or activation).
	 */
	public function save_default_values() {
		$defaults = $this->get_default_field_values();
		$existing_settings = $this->get_option();

		if ( $existing_settings ) {
			$defaults = apply_filters(
				$this->option_name . '_save_default_values',
				array_merge( $defaults, $existing_settings ),
				$this
			);

			delete_option( $this->option_name );
		}

		add_option( $this->option_name, $defaults );
	}

	/**
	 * Get all the sections that are in a settings tab
	 *
	 * @param string $tab The tab (Default is current tab)
	 * @return array
	 */
	public function get_tab_sections( $tab = '' ) {
		$tab = $tab ? $tab : $this->current_tab;
		$tab_sections = $tab . '_tab_sections';

		if ( method_exists( $this, $tab_sections ) ) {
			return apply_filters( $this->page_slug() . '_tab_sections', call_user_func( array( $this, $tab_sections ) ), $tab );
		}
	}

	public function add_settings_fields() {
		if ( !$this->option_name ) {
			return;
		}

		register_setting(
			$this->option_name,
			$this->option_name,
			array( $this, 'validate' )
		);

		$sections = $this->get_tab_sections();
		if ( !$sections ) {
			return;
		}

		foreach ( $sections as $key => $section ) {
			$page = isset( $section[ 'section' ] ) ? $section[ 'section' ] : $this->current_tab;
			add_settings_section(
				$key,
				isset( $section[ 'title' ] ) ? $section[ 'title' ] : '',
				array( $this, 'settings_section_description' ),
				$page
			);

			if ( !isset( $section[ 'fields' ] ) ) {
				continue;
			}

			foreach ( $section[ 'fields' ] as $field ) {
				if ( 'heading' === $field[ 'type' ] ) {
					$field[ 'id' ] = uniqid();
				}

				if ( !isset( $field[ 'id' ] ) || (isset( $field[ 'show_in_group' ] ) && $field[ 'show_in_group' ]) ) {
					continue;
				}

				$class = 'heading' === ($field[ 'type' ] ?? '') ? [ 'hidden' ] : [];

				if ( !empty( $field[ 'class' ] ) ) {
					$class = array_merge( $class, ( array ) $field[ 'class' ] );
				}

				if ( $this->is_pro_field( $field ) ) {
					$class[] = 'is-pro';
				}

				add_settings_field(
					$field[ 'id' ],
					$this->get_formatted_settings_field_label( $field ),
					array( $this, 'output_field' ),
					$page,
					$key,
					array(
						'class' => implode( ' ', $class ),
						'field' => $field,
						'fields' => $section[ 'fields' ]
					)
				);
			}
		}
	}

	/**
	 * Echo content at the top of the section, between the heading and field
	 */
	public function settings_section_description( $section ) {
		$tab_sections = $this->get_tab_sections();

		if ( isset( $tab_sections[ $section[ 'id' ] ] ) ) {
			if ( isset( $tab_sections[ $section[ 'id' ] ][ 'description' ] ) ) {
				echo "<div class='section-description'>" . wp_kses_post( $tab_sections[ $section[ 'id' ] ][ 'description' ] ) . '</div>';
			}
		}
	}

	public function is_pro_field( $field ) {
		return ($field[ 'pro' ] ?? false) && !$this->plugin_fn()->is_pro;
	}

	public function get_pro_version_text( $with_html = true ) {
		$text = __( 'PRO', 'nmerimedia' );
		return $with_html ? '<span class="nmerimedia-pro-version-text">(' . $text . ')</span>' : $text;
	}

	/**
	 * Format the label of a settings field before display
	 * This function is used to add error notification colors to the field label
	 * in situations where the field involved has an error
	 *
	 * @since 2.0.0
	 * @param type $field
	 */
	public function get_formatted_settings_field_label( $field ) {
		if ( !isset( $field[ 'label' ] ) ) {
			return '';
		}

		$label = $field[ 'label' ];

		if ( $this->is_pro_field( $field ) ) {
			$label = $label . ' ' . $this->get_pro_version_text();
		}

		if ( isset( $field[ 'error_codes' ] ) ) {
			$title = '';
			foreach ( $field[ 'error_codes' ] as $code ) {
				if ( $this->has_settings_error_code( $code ) ) {
					$title .= $this->get_error_message_by_code( $code );
				}
			}

			if ( !empty( $title ) ) {
				$label = '<span class="nmerimedia-settings-error" title="' . $title . '">' . $label . '</span>';
			}
		}

		if ( isset( $field[ 'desc_tip' ] ) ) {
			$label .= ' ' . $this->help_tip( $field[ 'desc_tip' ], ($field[ 'desc_tip_args' ] ?? [] ) );
		}

		return $label;
	}

	/**
	 * Check if particular settings error codes exists if we have errors after saving settings
	 *
	 * @since 2.0.0
	 * @param string|array $code Error code or array of error codes
	 * @return boolean
	 */
	public function has_settings_error_code( $code ) {
		foreach ( get_settings_errors( $this->page_slug() ) as $error ) {
			if ( in_array( $error[ 'code' ], ( array ) $code, true ) ) {
				return true;
			}
		}
		return false;
	}

	public function get_error_message_by_code( $code ) {
		$message = '';
		$codes_to_messages = $this->get_error_codes_to_messages();
		foreach ( ( array ) $code as $c ) {
			$message .= ($codes_to_messages[ $c ] ?? '') . '&#10;';
		}
		return trim( $message );
	}

	/**
	 * Get the name attribute of a form field based on the arguments supplied to the field
	 *
	 * @param array $field Arguments supplied to the field
	 */
	public function get_field_name( $field ) {
		if ( isset( $field[ 'name' ] ) ) {
			$name = $field[ 'name' ];
		} else {
			$key = isset( $field[ 'option_name' ] ) ? $field[ 'option_name' ] : (isset( $field[ 'id' ] ) ? $field[ 'id' ] : '');
			$name = $this->option_name . "[$key]";
		}

		$name = isset( $field[ 'option_group' ] ) && $field[ 'option_group' ] ? $name . '[]' : $name;
		return $name;
	}

	/**
	 * Get the value saved for a field in the database
	 *
	 * @param array $field Arguments supplied to the field
	 */
	public function get_field_value( $field ) {
		$field_default = isset( $field[ 'default' ] ) ? $field[ 'default' ] : '';
		$field_key = isset( $field[ 'option_name' ] ) ? $field[ 'option_name' ] : (isset( $field[ 'id' ] ) ? $field[ 'id' ] : '');
		$value = '';

		$get_option_func = array( $this, 'get_option' );

		if ( $field_key && is_callable( $get_option_func ) ) {
			$value = call_user_func( $get_option_func, $field_key, $field_default );
		} elseif ( isset( $field[ 'name' ] ) ) {
			$value = get_option( $field[ 'name' ], $field_default );
		}
		return $value;
	}

	/**
	 * Adds html checked attribute to a field if it should be checked
	 * Should be used for checkboxes, returns empty string otherwise.
	 *
	 * @param array $field Arguments supplied to the field
	 */
	public function checked( $field, $echo = false ) {
		$stored_value = ( array ) $this->get_field_value( $field );
		$field_value = isset( $field[ 'value' ] ) ? $field[ 'value' ] : 1;

		if ( in_array( $field_value, $stored_value ) ) {
			$result = " checked='checked'";
		} else {
			$result = '';
		}

		if ( $echo ) {
			echo esc_attr( $result );
		}
		return $result;
	}

	/**
	 * Adds html selected attribute to a select option if it should be selected
	 * Should be used for select option inputs, returns empty string otherwise.
	 *
	 * @param array $option_value The registered value for the option element
	 * @param array $field Arguments supplied to the field
	 */
	public function selected( $option_value, $field, $echo = false ) {
		$stored_value = ( array ) $this->get_field_value( $field );

		if ( in_array( $option_value, $stored_value ) ) {
			$result = " selected='selected'";
		} else {
			$result = '';
		}

		if ( $echo ) {
			echo esc_html( $result );
		}
		return $result;
	}

	public function output_field( $setting ) {
		$field = $setting[ 'field' ];
		$fields = $setting[ 'fields' ];

		// Ensure necessary fields are set
		$field_id = isset( $field[ 'id' ] ) ? esc_attr( $field[ 'id' ] ) : '';
		$field_type = isset( $field[ 'type' ] ) ? esc_attr( $field[ 'type' ] ) : '';
		$field_desc = isset( $field[ 'desc' ] ) ? wp_kses( $field[ 'desc' ], $this->allowed_post_tags() ) : '';
		$field_placeholder = isset( $field[ 'placeholder' ] ) ? esc_attr( $field[ 'placeholder' ] ) : '';
		$field_class = isset( $field[ 'class' ] ) ? esc_attr( $field[ 'class' ] ) : '';
		$field_css = isset( $field[ 'css' ] ) ? esc_attr( $field[ 'css' ] ) : '';
		$field_name = esc_attr( $this->get_field_name( $field ) );
		$raw_field_value = $this->get_field_value( $field );
		$field_value = !is_array( $raw_field_value ) ? esc_attr( $raw_field_value ) : $raw_field_value;
		$inline_class = isset( $field[ 'inline' ] ) && true === $field[ 'inline' ] ? 'nmerimedia-inline' : '';
		$field_options = isset( $field[ 'options' ] ) ? $field[ 'options' ] : array();
		$custom_attributes = array();

		if ( isset( $field[ 'custom_attributes' ] ) && is_array( $field[ 'custom_attributes' ] ) ) {
			foreach ( $field[ 'custom_attributes' ] as $attribute => $attribute_value ) {
				if ( false === $attribute_value ) {
					unset( $field[ 'custom_attributes' ][ $attribute ] );
					break;
				}
				$custom_attributes[] = $attribute . '="' . $attribute_value . '"';
			}
		}
		$field_custom_attributes = implode( ' ', $custom_attributes );

		if ( isset( $field[ 'show_in_group' ] ) && $field[ 'show_in_group' ] ) {
			return;
		}

		switch ( $field_type ) {
			case 'heading':
				echo '</td></tr></tbody></table>';
				echo isset( $field[ 'label' ] ) && !empty( $field[ 'label' ] ) ? "<h2 class='heading'>" .
					wp_kses( $field[ 'label' ], $this->allowed_post_tags() ) . '</h2>' : '';
				echo (!empty( $field_desc )) ? '<p>' . wp_kses( $field_desc, $this->allowed_post_tags() ) . '</p>' : '';
				echo '<table class="form-table" role="presentation"><tbody><tr class="hidden"><th></th><td>';
				break;

			case 'text':
			case 'password':
			case 'number':
				printf( "<input type='%s' id='%s' name='%s' size='40' value='%s' placeholder='%s' %s />",
					esc_attr( $field_type ),
					esc_attr( $field_id ),
					esc_attr( $field_name ),
					esc_attr( $field_value ),
					esc_attr( $field_placeholder ),
					wp_kses( $field_custom_attributes, [] )
				);
				break;

			case 'textarea':
				printf( "<textarea name='%s' cols='45' rows='4' placeholder='%s'>%s</textarea>",
					esc_attr( $field_name ),
					esc_attr( $field_placeholder ),
					wp_kses_post( $field_value )
				);
				break;

			case 'checkbox':
				if ( isset( $field[ 'checkboxgroup' ] ) ) {
					$group_fields = array_filter( $fields, function( $f ) use( $field ) {
						return isset( $f[ 'checkboxgroup' ] ) && $f[ 'checkboxgroup' ] == $field[ 'checkboxgroup' ];
					} );

					if ( $group_fields ) {
						foreach ( $group_fields as $group_field ) {
							printf( "<label><input %s value='%s' name='%s' type='checkbox' /> %s</label><br />",
								esc_attr( $this->checked( $group_field ) ),
								isset( $group_field[ 'value' ] ) ? esc_attr( $group_field[ 'value' ] ) : 1,
								esc_attr( $this->get_field_name( $group_field ) ),
								wp_kses( $group_field[ 'desc' ] ?? null, $this->allowed_post_tags() )
							);
						}
					}
				} else {
					printf( "<label><input %s value='1' name='%s' type='checkbox' %s /> %s</label>",
						esc_attr( $this->checked( $field ) ),
						esc_attr( $field_name ),
						wp_kses( $field_custom_attributes, [] ),
						wp_kses( $field_desc, $this->allowed_post_tags() )
					);
				}
				break;

			case 'radio':
				?>
				<div class="nmerimedia-input-group <?php echo esc_attr( $inline_class ); ?>">
					<?php
					foreach ( $field[ 'options' ] as $key => $val ) :
						$checked = checked( $key, $field_value, false );
						?>
						<div><label><input <?php echo esc_attr( $checked ) . ' ' . wp_kses( $field_custom_attributes, [] ); ?>
									value="<?php echo esc_attr( $key ); ?>"
									name="<?php echo esc_attr( $field_name ); ?>"
									type="radio"/><?php echo wp_kses( $val, $this->allowed_post_tags() ); ?></label></div>
						<?php endforeach; ?>
				</div>
				<?php
				break;

			case 'radio_with_image':
				?>
				<div class="nmerimedia-btn-group nmerimedia-input-group <?php echo esc_attr( $inline_class ); ?>">
					<?php
					foreach ( $field[ 'options' ] as $key => $args ) :
						$checked = checked( $key, $field_value, false );
						$option_id = "{$field_id}-{$key}";
						?>
						<div class="nmerimedia-btn <?php echo $this->is_pro_field( $args ) ? 'is-pro' : ''; ?>">
							<input <?php echo esc_attr( $checked ); ?>
								id="<?php echo esc_attr( $option_id ); ?>"
								type="radio"
								value="<?php echo esc_attr( $key ); ?>"
								name="<?php echo esc_attr( $field_name ); ?>">
							<label for="<?php echo esc_attr( $option_id ); ?>"
										 title="<?php echo isset( $args[ 'label_title' ] ) ? esc_attr( $args[ 'label_title' ] ) : ''; ?>"
										 class="nmerimedia-tip">
								<?php
								echo isset( $args[ 'image' ] ) ? wp_kses( $args[ 'image' ], $this->allowed_post_tags() ) : '';
								echo isset( $args[ 'label' ] ) ? wp_kses( $args[ 'label' ], $this->allowed_post_tags() ) : '';
								?>
							</label>
						</div>
					<?php endforeach; ?>
				</div>
				<?php
				break;

			case 'select':
				printf( "<select class='%s' name='%s' id='%s' %s>",
					esc_attr( $field_class ),
					esc_attr( $field_name ),
					esc_attr( $field_id ),
					wp_kses( $field_custom_attributes, [] )
				);
				foreach ( $field_options as $key => $val ) {
					printf( "<option value='%s' %s>%s</option>",
						esc_attr( $key ),
						esc_attr( $this->selected( $key, $field ) ),
						esc_html( $val )
					);
				}
				echo '</select>';
				break;

			case 'select_page':
				$args = array(
					'name' => esc_attr( $field_name ),
					'id' => esc_attr( $field_id ),
					'sort_column' => 'menu_order',
					'sort_order' => 'ASC',
					'show_option_none' => ' ',
					'class' => esc_attr( $field_class ),
					'echo' => false,
					'selected' => absint( $field_value ),
				);

				if ( isset( $field[ 'args' ] ) ) {
					$args = wp_parse_args( $field[ 'args' ], $args );
				}

				$html = str_replace( ' id=', " data-placeholder='" . esc_attr( $field_placeholder ) . "' style='" . esc_attr( $field_css ) . "' class='" . esc_attr( $field_class ) . "' id=", wp_dropdown_pages( $args ) );

				echo $html; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

				break;


			default:
				break;
		}

		// These fields should not have description
		$exclude_fields = array( 'checkbox', 'heading' );
		if ( $field_desc && !in_array( $field_type, $exclude_fields ) ) {
			echo '<p>' . wp_kses( $field_desc, $this->allowed_post_tags() ) . '</p>';
		}
	}

	// Validate fields before save
	public function validate( $input ) {
		$referer = array();
		parse_str( wp_parse_url( wp_get_referer(), PHP_URL_QUERY ), $referer );

		// We're only dealing with fields posted from a particular tab or section
		$tab = $this->get_current_tab( $referer );
		$section = $this->get_current_section( $referer );

		$tab_sections = $this->get_tab_sections( $tab );

		if ( !$tab_sections ) {
			return $input;
		}

		$fields = array();

		foreach ( $tab_sections as $content ) {
			foreach ( $content as $prop => $value ) {
				if ( 'fields' == $prop ) {
					if ( $section ) {
						if ( isset( $content[ 'section' ] ) && $content[ 'section' ] === $section ) {
							$fields = array_merge( $fields, $value );
							break 2;
						}
					} else {
						$fields = array_merge( $fields, $value );
					}
				}
			}
		}

		foreach ( $fields as $field ) {
			// Key for the field
			$key = isset( $field[ 'option_name' ] ) ? $field[ 'option_name' ] : (isset( $field[ 'id' ] ) ? $field[ 'id' ] : '');

			// Get posted value
			$posted_value = isset( $input[ $key ] ) ? $input[ $key ] : null;

			// If this field value should be an array but the posted value is empty, convert it to array
			if ( !$posted_value && ($field[ 'option_group' ] ?? false) ) {
				$posted_value = [];
			}

			// Format/Sanitize value to save in database
			if ( isset( $field[ 'type' ] ) ) {
				switch ( $field[ 'type' ] ) {
					case 'textarea':
						$value = wp_kses( trim( $posted_value ), $this->allowed_post_tags() );
						break;

					default:
						$value = $this->sanitize( $posted_value );
						break;
				}
			}

			$input[ $key ] = $value;
		}


		/**
		 * Set settings errors here
		 */
		$f_input = apply_filters( $this->page_slug() . '_validate_input', $input, $this );

		$options = array_merge( $this->get_option(), $f_input );

		do_action( $this->page_slug() . '_before_save_input', $input, $this );

		if ( get_settings_errors( $this->page_slug() ) ) {
			add_settings_error( $this->page_slug(), 'settings-saved', __( 'Settings saved.', 'nmerimedia' ), 'success' );
		}

		$current_section_error_codes = $this->get_current_section_error_codes( $referer );

		foreach ( $current_section_error_codes as $k => $code ) {
			if ( $this->has_settings_error_code( $code ) ) {
				unset( $current_section_error_codes[ $k ] );
			}
		}

		$this->delete_settings_errors( $current_section_error_codes );

		return $options;
	}

	/**
	 * Get the fields for the current settings section being viewed
	 *
	 * @param array $request The associative array used to determine the tab, typically $_GET or HTTP_REFERER
	 * @return array
	 */
	public function get_current_section_fields( $request = array() ) {
		$tab = $this->get_current_tab( $request );
		$section = $this->get_current_section( $request );
		$tab_sections = $this->get_tab_sections( $tab );
		$fields = array();

		if ( $tab_sections ) {
			foreach ( $tab_sections as $content ) {
				foreach ( $content as $prop => $value ) {
					if ( 'fields' == $prop ) {
						if ( $section ) {
							if ( isset( $content[ 'section' ] ) && $content[ 'section' ] === $section ) {
								$fields = array_merge( $fields, $value );
								break 2;
							}
						} else {
							$fields = array_merge( $fields, $value );
						}
					}
				}
			}
		}
		return $fields;
	}

	/**
	 * Get the error codes available for all the fields in a settings section
	 * @param array $request The associative array used to determine the tab, typically $_GET or HTTP_REFERER
	 */
	public function get_current_section_error_codes( $request = array() ) {
		$error_codes = array();

		foreach ( $this->get_current_section_fields( $request ) as $field ) {
			if ( isset( $field[ 'error_codes' ] ) ) {
				$error_codes = array_merge( $error_codes, $field[ 'error_codes' ] );
			}
		}
		return array_unique( $error_codes );
	}

	/**
	 * Save a settings error to the database and also register it to be
	 * shown to the user using 'add_settings_error()'.
	 *
	 * @param string $code Settings error code
	 * @param string $message Settings error message to display to user
	 * @param string $type Type of error. Default warning.
	 */
	public function save_settings_error( $code, $message, $type = 'warning' ) {
		add_settings_error( $this->page_slug(), $code, $message, $type );
		$saved = get_option( $this->get_settings_errors_key(), array() );
		$saved[ $code ] = array( 'type' => $type );
		update_option( $this->get_settings_errors_key(), $saved );
	}

	/**
	 * Delete settings errors saved to the database
	 *
	 * @param array $codes Codes for settings errors to delete
	 */
	public function delete_settings_errors( $codes ) {
		if ( !empty( $codes ) ) {
			$saved = get_option( $this->get_settings_errors_key(), array() );
			foreach ( $codes as $code ) {
				if ( isset( $saved[ $code ] ) ) {
					unset( $saved[ $code ] );
				}
			}
			update_option( $this->get_settings_errors_key(), $saved );
		}
	}

	/**
	 * Get the settings errors that have been saved to the database
	 * @return array
	 */
	public function get_saved_settings_errors() {
		return get_option( $this->get_settings_errors_key(), array() );
	}

	public function show_saved_settings_errors() {
		$error_codes = $this->get_current_section_error_codes();
		if ( !empty( $error_codes ) ) {
			$saved_settings_errors = $this->get_saved_settings_errors();

			foreach ( $error_codes as $code ) {
				if ( !$this->has_settings_error_code( $code ) && array_key_exists( $code, $saved_settings_errors ) ) {
					$error = $saved_settings_errors[ $code ];
					$error_msg = $this->get_error_message_by_code( $code );
					add_settings_error( $this->page_slug(), $code, $error_msg, $error[ 'type' ] );
				}
			}
		}
	}

	public function get_error_codes_to_messages() {
		return [];
	}

	/**
	 * Search for woocommerce products to be used in settings fields.
	 */
	public function search_products() {
		check_ajax_referer( 'nmerimedia_search_products' );

		if ( !function_exists( 'wc' ) ) {
			wp_die();
		}

		$term = '';
		$include_variations = true;

		if ( empty( $term ) && isset( $_GET[ 'term' ] ) ) {
			$term = ( string ) wc_clean( wp_unslash( $_GET[ 'term' ] ) );
		}

		if ( empty( $term ) ) {
			wp_die();
		}

		$limit = !empty( $_GET[ 'limit' ] ) ? absint( $_GET[ 'limit' ] ) : 30;

		$include_ids = !empty( $_GET[ 'include' ] ) ? array_map( 'absint', ( array ) wp_unslash( $_GET[ 'include' ] ) ) : array();
		$exclude_ids = !empty( $_GET[ 'exclude' ] ) ? array_map( 'absint', ( array ) wp_unslash( $_GET[ 'exclude' ] ) ) : array();

		$exclude_types = array();
		if ( !empty( $_GET[ 'exclude_type' ] ) ) {
// Support both comma-delimited and array format inputs.
			$exclude_types = wp_unslash( $_GET[ 'exclude_type' ] );
			if ( !is_array( $exclude_types ) ) {
				$exclude_types = explode( ',', $exclude_types );
			}

// Sanitize the excluded types against valid product types.
			foreach ( $exclude_types as &$exclude_type ) {
				$exclude_type = strtolower( trim( $exclude_type ) );
			}
			$exclude_types = array_intersect(
				array_merge( array( 'variation' ), array_keys( wc_get_product_types() ) ),
				$exclude_types
			);
		}

		$data_store = \WC_Data_Store::load( 'product' );
		$ids = $data_store->search_products( $term, '', ( bool ) $include_variations, false, $limit, $include_ids, $exclude_ids );
		$products = array();

		foreach ( $ids as $id ) {
			$product_object = wc_get_product( $id );

			if ( !wc_products_array_filter_readable( $product_object ) ) {
				continue;
			}

			$formatted_name = $product_object->get_formatted_name();
			$managing_stock = $product_object->managing_stock();

			if ( in_array( $product_object->get_type(), $exclude_types, true ) ) {
				continue;
			}

			if ( $managing_stock && !empty( $_GET[ 'display_stock' ] ) ) {
				$stock_amount = $product_object->get_stock_quantity();
				$formatted_name .= ' &ndash; ' . sprintf(
						/* translators: %d: stock quantity */
						__( 'Stock: %d', 'nmerimedia' ), wc_format_stock_quantity_for_display( $stock_amount, $product_object ) );
			}

			$products[ $product_object->get_id() ] = rawurldecode( wp_strip_all_tags( $formatted_name ) );
		}

		wp_send_json( $products );
	}

	/**
	 * Get the call to action for buying the pro version of the plugin
	 *
	 * Displays the first five pro features of the plugin from the readme.txt file
	 * with a buy link
	 * @return string
	 */
	public function get_buy_pro_notice() {
		$readme = $this->get_readme_file( $this->plugin_fn()->path );
		$features = $readme ? $readme->get_pro_version_features( true ) : '';
		if ( empty( $features ) ) {
			return;
		}

		$img_path = $this->plugin_fn()->path . '/assets/img/logo.png';
		$img_url = $this->plugin_fn()->url . '/assets/img/logo.png';
		$image = file_exists( $img_path ) ? "<img style='width:32px;height:auto;' src='$img_url'>" : '';

		$message = '<table><tbody><tr><td>' . $image . '</td><td><strong>' . $this->plugin_fn()->name . '</strong></td></tr></tbody></table><br>';
		$message .= __( 'You are using the free version of the plugin. Get the pro version to enable these features and more:', 'nmerimedia' );
		$message .= '<ol>';

		// Get first five features
		foreach ( array_slice( $features, 0, 5 ) as $feature ) {
			$message .= '<li>' . $feature . '</li>';
		}

		$message .= '</ol>';

		if ( $this->plugin_fn()->product_url ) {
			$message .= '<a class="button button-primary" target="_blank" href="' . $this->plugin_fn()->product_url . '">' . __( 'Get PRO', 'nmerimedia' ) . '</a><br><br>';
		}

		return $message;
	}

}
