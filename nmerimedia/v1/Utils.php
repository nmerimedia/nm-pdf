<?php

namespace NmeriMedia\V1;

use NmeriMedia\V1\Traits\Utils as Functions;

defined( 'ABSPATH' ) || exit;

/**
 * Main utils class
 * @class \NmeriMedia\V1\Utils
 */
class Utils {

	use Functions;

	/**
	 * Get the plugin path to the nmerimedia directory
	 * @return string
	 */
	public static function path() {
		return plugin_dir_path( __DIR__ );
	}

	/**
	 * Get the plugin url to the nmerimedia directory
	 * @return string
	 */
	public static function url() {
		return plugin_dir_url( __DIR__ );
	}

	/**
	 * Get the nmerimedia version
	 * @return system
	 */
	public static function version() {
		return __NAMESPACE__;
	}

}
