<?php
defined( 'ABSPATH' ) || exit;


do_action( 'nm_before_component', $this );
?>

<div <?php echo wp_kses( $this->get_container_attributes( true ), [] ); ?>>
	<div <?php echo wp_kses( $this->get_body_attributes( true ), [] ); ?>>
		<?php echo wp_kses( $this->get_body(), \NmeriMedia\V1\Utils::allowed_post_tags() ); ?>
	</div>
	<?php if ( !$this->has_close_button_hidden() ) : ?>
		<button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="<?php esc_html_e( 'Close', 'nmerimedia' ); ?>"></button>
	<?php endif; ?>
</div>

<?php do_action( 'nm_after_component', $this ); ?>