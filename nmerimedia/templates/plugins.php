<?php

defined( 'ABSPATH' ) || exit;


return [
	'nm-gift-registry' => [
		'name' => __( 'NM Gift Registry and Wishlist', 'nmerimedia' ),
		'image_url' => \NmeriMedia\V1\Utils::url() . 'assets/img/nm-gift-registry-logo.png',
		'image_path' => \NmeriMedia\V1\Utils::path() . 'assets/img/nm-gift-registry-logo.png',
		'desc' => __( 'Allow users to create professional gift registries and wishlists for your WooCommerce store.', 'nmerimedia' ),
		'product_url' => 'https://nmerimedia.com/product/nm-gift-registry/',
	],
	'nm-gift-registry-crowdfunding' => [
		'name' => __( 'Crowdfunding - NM Gift Registry and Wishlist', 'nmerimedia' ),
		'image_url' => \NmeriMedia\V1\Utils::url() . 'assets/img/nm-gift-registry-crowdfunding-logo.png',
		'image_path' => \NmeriMedia\V1\Utils::path() . 'assets/img/nm-gift-registry-crowdfunding-logo.png',
		'desc' => __( 'Allows wishlist items to be crowdfunded and enables wishlists to receive free contributions not attached to products. Enables the use of special coupons for wishlists.', 'nmerimedia' ),
		'product_url' => 'https://nmerimedia.com/product/crowdfunding-nm-gift-registry-and-wishlist/'
	],
	'nmerimedia' => [
		'name' => __( 'NM Multiple WooCommerce Addresses', 'nmerimedia' ),
		'image_url' => \NmeriMedia\V1\Utils::url() . 'assets/img/nm-multi-addresses.png',
		'image_path' => \NmeriMedia\V1\Utils::path() . 'assets/img/nm-multi-addresses.png',
		'desc' => __( 'Create multiple billing and shipping addresses for checkout. Ship cart items to multiple addresses. Split cart items into separate shipping packages. Restrict shipping to specific destinations.', 'nmerimedia' ),
		'product_url' => 'https://nmerimedia.com/product/nm-multiple-woocommerce-addresses/'
	],
	'nm-pdf' => [
		'name' => __( 'NM PDF', 'nmerimedia' ),
		'image_url' => \NmeriMedia\V1\Utils::url() . 'assets/img/nm-pdf.png',
		'image_path' => \NmeriMedia\V1\Utils::path() . 'assets/img/nm-pdf.png',
		'desc' => __( 'Create pdf invoices and packing-slips from woocommerce orders', 'nmerimedia' ),
		'product_url' => 'https://nmerimedia.com/product/nm-pdf/'
	],
];
