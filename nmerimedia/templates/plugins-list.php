<?php
defined( 'ABSPATH' ) || exit;

if ( !isset( $on_sale_plugins ) ) {
	return;
}
?>

<div class="nmps">
	<h3 class="nm-text-gradient"><?php esc_html_e( 'Get more plugins...', 'nmerimedia' ); ?></h3>
	<?php foreach ( $on_sale_plugins as $sale_plugin ) : ?>
		<a target="_blank" href="<?php echo esc_attr( $sale_plugin[ 'product_url' ] ); ?>" class="nmp nm" data-bs-toggle="popover" data-bs-placement="left" data-bs-trigger="hover" data-bs-content="<?php echo esc_attr( $sale_plugin[ 'desc' ] ?? null  ); ?>">
			<div class="plugin-title-container">
				<?php $src = file_exists( $sale_plugin[ 'image_path' ] ) ? $sale_plugin[ 'image_url' ] : ''; ?>
				<img class="plugin-image" src="<?php echo esc_attr( $src ); ?>">
				<p class="plugin-title"><?php echo esc_html( $sale_plugin[ 'name' ] ); ?></p>
			</div>
		</a>
	<?php endforeach; ?>
</div>
