<?php
defined( 'ABSPATH' ) || exit;


do_action( 'nm_before_component', $this );
?>

<<?php echo esc_attr( $this->get_tag() ) . ' ' . wp_kses( $this->get_container_attributes( true ), [] ); ?>>
<?php echo wp_kses( $this->get_text(), \NmeriMedia\V1\Utils::allowed_post_tags() ); ?>
</<?php echo esc_attr( $this->get_tag() ); ?>>


<?php do_action( 'nm_after_component', $this ); ?>
