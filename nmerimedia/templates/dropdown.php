<?php
defined( 'ABSPATH' ) || exit;


do_action( 'nm_before_component', $this );
?>

<div <?php echo wp_kses( $this->get_container_attributes( true ), [] ); ?>>

	<?php if ( $this->is_split() ) : ?>
		<button type="button" class="button"><?php echo wp_kses( $this->get_text(), \NmeriMedia\V1\Utils::allowed_post_tags() ); ?></button>
	<?php endif; ?>

	<a <?php echo wp_kses( $this->get_toggle_attributes(), [] ); ?>>
		<?php if ( $this->is_split() ) : ?>
			<span class="visually-hidden"><?php esc_html_e_( 'Toggle Dropdown', 'nmerimedia' ); ?></span>
			<?php
		else:
			echo wp_kses( $this->get_text(), \NmeriMedia\V1\Utils::allowed_post_tags() );
		endif;
		?>
	</a>

	<?php if ( !empty( $this->get_menu_content() ) ) : ?>
		<div <?php echo wp_kses( $this->get_menu_attributes(), [] ); ?>>
			<?php echo wp_kses( $this->get_menu_content(), \NmeriMedia\V1\Utils::allowed_post_tags() ); ?>
		</div>

	<?php elseif ( !empty( $this->get_menu_items() ) ) : ?>
		<ul <?php echo wp_kses( $this->get_menu_attributes(), [] ); ?>>
			<?php foreach ( $this->get_menu_items() as $item ): ?>
				<li><?php echo wp_kses( $this->get_menu_item_html( $item ), \NmeriMedia\V1\Utils::allowed_post_tags() ); ?></li>
			<?php endforeach; ?>
		</ul>

	<?php endif; ?>

</div>

<?php do_action( 'nm_after_component', $this ); ?>