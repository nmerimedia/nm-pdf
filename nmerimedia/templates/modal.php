<?php
defined( 'ABSPATH' ) || exit;
do_action( 'nm_before_component', $this );
?>

<div <?php echo wp_kses( $this->get_container_attributes( true ), [] ); ?>>
  <div <?php echo wp_kses( $this->get_dialog_attributes( true ), [] ); ?>>
    <div <?php echo wp_kses( $this->get_content_attributes( true ), [] ); ?>>


			<?php if ( !$this->has_header_hidden() ): ?>
				<div <?php echo wp_kses( $this->get_header_attributes( true ), [] ); ?>>
					<?php if ( !$this->has_title_hidden() ) : ?>
						<div>
							<?php echo wp_kses_post( $this->get_title() ); ?>
						</div>
					<?php endif; ?>

					<?php if ( !$this->has_header_close_button_hidden() ) : ?>
						<button type="button" class="nm-close btn-close" data-bs-dismiss="modal" aria-label="<?php esc_html_e( 'Close', 'nmerimedia' ); ?>"></button>
					<?php endif; ?>
				</div>
			<?php endif; ?>



      <div <?php echo wp_kses( $this->get_body_attributes( true ), [] ); ?>>
				<?php echo $this->get_body(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
      </div>


			<?php if ( !$this->has_footer_hidden() && $this->get_footer() ) : ?>
				<div <?php echo wp_kses( $this->get_footer_attributes( true ), [] ); ?>>
					<?php echo $this->get_footer(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
				</div>
			<?php endif; ?>


    </div>
  </div>
</div>

<?php do_action( 'nm_after_component', $this ); ?>