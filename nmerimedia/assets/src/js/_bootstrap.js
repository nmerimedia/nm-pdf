import Collapse from '/var/www/bootstrap-5.0.2/js/src/collapse'
import Dropdown from '/var/www/bootstrap-5.0.2/js/src/dropdown'
import Modal from '/var/www/bootstrap-5.0.2/js/src/modal'
import Offcanvas from '/var/www/bootstrap-5.0.2/js/src/offcanvas'
import Popover from '/var/www/bootstrap-5.0.2/js/src/popover'
import Tab from '/var/www/bootstrap-5.0.2/js/src/tab'
import Toast from '/var/www/bootstrap-5.0.2/js/src/toast'
import Tooltip from '/var/www/bootstrap-5.0.2/js/src/tooltip'

		const Author = 'Nmeri Media'
const Desc = 'Boostrap components'
const Version = '5.0.2'

export default {
	Author,
	Desc,
	Version,
	Collapse,
	Dropdown,
	Modal,
	Offcanvas,
	Popover,
	Tab,
	Toast,
	Tooltip
}