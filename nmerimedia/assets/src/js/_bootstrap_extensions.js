
function block(selector) {
	if (jQuery.blockUI) {
		jQuery(selector).addClass('is-nm-blocked').block({
			message: null,
			overlayCSS: {
				backgroundColor: '#fff'
			},
			css: {
				backgroundColor: 'transparent',
				border: 'none'
			}
		});
	}
}

function unblock(selector) {
	if (jQuery.blockUI) {
		jQuery(selector).removeClass('is-nm-blocked').unblock();
	}
}

/**
 * Appends a dialog element to the document body to be shown later
 * @param {string} template The dialog template
 * @returns {string|null} The id of the dialog template that can be used to trigger
 * the show() method on it later, or null if the insert operation is simply a reload
 * of the template from the server.
 *
 * The id is not returned if the insert operation is a reload to prevent the template
 * from being shown by the showTemplate() function as this is the function in which the
 * insertTemplate() function is called.
 */
function insertTemplate(template) {
	var d = new DOMParser().parseFromString(template, 'text/html');
	var id = d.querySelector('body').firstChild.getAttribute('id');
	var isReload = false;

	// Remove previous identitcal component from dom
	var prevComp = document.getElementById(id);
	if (prevComp) {
		isReload = prevComp.dataset.isReload ? true : false;
		prevComp.remove();
	}

	document.body.appendChild(d.querySelector('body').children[0]);
	return {
		isReload: isReload,
		id: id
	};
}

/**
 * Show a dialog template
 * @param {string} template Template to show in a dialog
 * @param {object} args Arguments used to compose the template. "component" key is
 * required with a value.
 * @returns {undefined}
 */
function showTemplate(template, args) {
	// insert template into dom
	var inserted = NM.insertTemplate(template);

	// Set component related target
	var dialog = document.getElementById(inserted.id);
	var comp = new NM[NM.getDialogComponentName(dialog)](dialog);
	var relatedTarget;

	if (args.btn && args.btn.dataset.nmId) {
		relatedTarget = args.btn.dataset.nmId;
	}

	comp.relatedTarget = relatedTarget;

	// show template
	if (!inserted.isReload) {
		comp.show(relatedTarget || null);
	}
}

function showToast(notice) {
	var toastDiv = jQuery('.nm-component.toast-container');
	if (toastDiv.length) {
		var notices = notice instanceof Array ? notice : [notice];

		notices.forEach(function (el) {
			var html = jQuery(el)[0];
			toastDiv.append(html); // append to start at bottom so that it would stack according to last
			(new NM.Toast(html)).show();
		});
	}
}

/**
 * Get the component used as a dialog
 * @param {Element} dialog The dialog element
 * @returns {String} The component name, Modal or Offcanvas
 */
function getDialogComponentName(dialog) {
	return dialog.classList.contains('modal') ? 'Modal' : 'Offcanvas';
}

/**
 * Close the disalog
 * @param {Element} dialog The dialog element
 * @param {boolean} force Whether to simply close the dialog only and perform no other action related
 * to closing. Default false;
 */
function closeDialog(dialog, force) {
	/**
	 * In closing the currently opened dialog, we can show another dialog
	 * based on the data attributes supplied to the currently opened dialog.
	 *
	 * The data attributes must have values equal to the ids of elements in the dom.
	 * These elements in most cases should be dialog elements.
	 *
	 * If the dialog has any of the following data attributes, the corresponding actions would
	 * be performed:
	 *
	 * nm-redirect:
	 * This opens a new dialog. First it checks if the dialog has already been previously opened and
	 * the instance exists. If so, it searches the instance for the button clicked to open the dialog
	 * (the relatedTarget property). If this is founds it triggers a click on the button again to reload the
	 * dialog. If it doesn't find the button, it takes the value supplied to the data attribute as the id of a
	 * clickable button to open the new dialog and triggers a click on it.
	 *
	 * nm-back:
	 * This simply shows a dialog that has already been previously shown and it's instance exists in the dom,
	 * by simply invoking the show() method on the instance again. In this case, the dialog is not refreshed.
	 */

	var component = NM.getDialogComponentName(dialog);

	if (force) {
		NM[component].getInstance(document.getElementById(dialog.getAttribute('id'))).hide();
		return;
	}

	if (dialog.dataset.nmRedirect) {
		var target = document.querySelector(dialog.dataset.nmRedirect);
		var newDialog = NM[component].getInstance(target);
		if (newDialog && newDialog.relatedTarget) {
			document.querySelector('[data-nm-id="' + newDialog.relatedTarget + '"]').click();
		} else if (target) {
			target.click();
		}
	} else if (dialog.dataset.nmBack) {
		NM[component].getInstance(document.querySelector(dialog.dataset.nmBack)).show();
	} else if (dialog.dataset.nmClose) {
		NM[component].getInstance(document.getElementById(dialog.getAttribute('id'))).hide();
	}
}

// Replace templates in the dom when they have been retrieved from the server
function replaceTemplates(templates) {
	for (var key in templates) {
		var collection = document.querySelectorAll(key);
		if (collection) {
			var template = templates[key];
			collection.forEach(function (coll) {
				var wrap = document.createElement('div');
				wrap.innerHTML = template;
				var newTemplate = wrap.children[0];
				coll.replaceWith(newTemplate);
				jQuery(document.body).trigger('nm_replaced_template', {
					key: key,
					template: newTemplate
				});
			});
		}
	}
}

/**
 * Retrieve dialog templates from the server to be loaded in the dom.
 *
 * This function is used to update dialogs that are already in the dom which cannot
 * be updated using data-nm-redirect or replaceTemplates() function as they are not
 * directly connected to the current dialog being shown.
 *
 * This function is called after a dialog operation has been successful (for example when
 * the form in the dialog is saved to the database). We loop through all the dialogs in the
 * dom to see if any of them should be reloaded. This is determined by a 'data-nm-reload'
 * attribute on the dialog containing the id of the currently shown dialog as it value.
 *
 * If we find such dialog, we simply reload it by triggering a click on its 'relatedTarget'
 * property which has to be set.
 *
 * @param {Element} dialog
 */
function reloadTemplates(dialog) {
	var collection = document.querySelectorAll('.dialog[data-nm-reload]');
	if (collection.length) {
		var component = NM.getDialogComponentName(dialog);
		var dialogId = dialog.getAttribute('id');

		collection.forEach(function (coll) {
			var reloads = JSON.parse(coll.dataset.nmReload);
			if (reloads.indexOf('#' + dialogId) > -1) {
				var newDialog = NM[component].getInstance(coll);
				if (newDialog && newDialog.relatedTarget) {
					coll.dataset.isReload = true;
					document.querySelector('[data-nm-id="' + newDialog.relatedTarget + '"]').click();
				}
			}
		});
	}
}

/**
 * Perform standard operations with the response from an ajax request
 */
function processResponse(response, args) {
	if (response.log) {
		console.log(response);
	}

	if (args && args.blocked) {
		NM.unblock(args.blocked);
	}

	if (response.success && args && args.dialog) {
		NM.closeDialog(args.dialog);
		NM.reloadTemplates(args.dialog);
	}

	if (response.show_template) {
		NM.showTemplate(response.show_template, args);
	}

	if (response.notice) {
		NM.showToast(response.notice);
	}

	if (response.replace_templates) {
		NM.replaceTemplates(response.replace_templates);
	}
}

export {
block,
		closeDialog,
		getDialogComponentName,
		insertTemplate,
		processResponse,
		reloadTemplates,
		replaceTemplates,
		showTemplate,
		showToast,
		unblock
		};