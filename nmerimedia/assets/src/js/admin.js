jQuery(function ($) {

	//= include _bootstrap-modal-select2-hack.js

	/**
	 * Initialize a specific bootstrap component
	 * @param {string} component The name of the component e.g. tooltip, popover
	 * @param {object} element The element to initialize the component on. Defaults to document
	 */
	function initComponent(component, element) {
		if ('undefined' !== typeof component && 'undefined' !== typeof NM) {
			var componentType = component.charAt(0).toUpperCase() + component.slice(1);
			var el = element instanceof Element ? element : document;
			var List = [].slice.call(el.querySelectorAll('.nm[data-bs-toggle="' + component + '"]'));
			var options = {};

			if ('tooltip' === component || 'popover' === component) {
				options['html'] = true;
				options['customClass'] = 'nm-component';
			}

			List.map(function (triggerEl) {
				return new NM[componentType](triggerEl, options);
			});
		}
	}

	initComponent('tooltip', document);
	initComponent('popover', document);


	/**
	 * Initializes all select2 elements used by plugin and retrieves their data
	 */
	var nm_select2 = {
		init: function () {
			try {
				$(document.body)
						.on('nm-select2-init', this.activate_select2)
						.trigger('nm-select2-init');
			} catch (e) {
				window.console.log(e);
			}
		},

		activate_select2: function () {
			nm_select2.select2_on_regular_select_boxes();
			nm_select2.select2_product_search();
		},

		select2_on_regular_select_boxes: function () {
			$(':input.nm-select2').filter(':not(.enhanced)').each(function () {
				var select2_args = {
					minimumResultsForSearch: 10,
					allowClear: $(this).data('allow_clear') ? true : false,
					placeholder: $(this).data('placeholder')
				};

				$(this).select2(select2_args).addClass('enhanced');
			});
		},

		select2_product_search: function () {
			$(':input.nm-product-search').each(function () {
				var select2_args = {
					allowClear: $(this).data('allow_clear') ? true : false,
					placeholder: $(this).data('placeholder'),
					minimumInputLength: $(this).data('minimum_input_length') ? $(this).data('minimum_input_length') : '3',
					escapeMarkup: function (m) {
						return m;
					},
					ajax: {
						url: $(this).data('ajax_url'),
						dataType: 'json',
						delay: 250,
						data: function (params) {
							return {
								term: params.term,
								action: 'nmerimedia_search_products',
								_wpnonce: $(this).data('nonce'),
								exclude: $(this).data('exclude'),
								exclude_type: $(this).data('exclude_type'),
								include: $(this).data('include'),
								limit: $(this).data('limit'),
								display_stock: $(this).data('display_stock'),
							};
						},
						processResults: function (data) {
							var terms = [];
							if (data) {
								$.each(data, function (id, text) {
									terms.push({
										id: id,
										text: text
									});
								});
							}
							return {
								results: terms
							};
						},
						cache: true
					}
				};

				$(this).select2(select2_args);

			});
		},
	};

	nm_select2.init();
});