/**
 * Hacks to make select2 work well in bootstrap modal and offcanvas
 * (Drop-in plugin)
 */

jQuery(document.body)
		.on('select2:open', '.nm-component.dialog .select2-hidden-accessible', increaseSelect2DropdownZIndexForVisibility)
		.on('select2:opening', '.nm-component.dialog .select2-hidden-accessible', removeTabIndex)
		.on('select2:closing', '.nm-component.dialog .select2-hidden-accessible', restoreTabIndex)
		.on('select2:opening', '.nm-component.dialog', closeSelect2OnScroll);


/**
 * Select2 dropdown is attached to body and so does not show in bootstrap component.
 * Increase the z-index to make it appear above the bootstrap component.
 * (This hack seems to be necessary only for the modal component)
 * @description select2-bootstrap component HACK
 */
function increaseSelect2DropdownZIndexForVisibility() {
	/**
	 * We target the last because it is the one attached to the body.
	 * Targeting others would result in bugs on the checkout page
	 * Bootstrap 5 modal z-index is 1040
	 */
	$('.select2-container').last().css('z-index', 1111);
}

/**
 * If the bootstrap component is instantiated with focus set to true (which is the default)
 * and it has tabindex = -1, it would be impossible to type into the select2 dropdown
 * search when open. To correct this we simply remove the tabindex attribute from the
 * bootstrap component when the dropdown is open.
 * @description select2-bootstrap component HACK
 */
function removeTabIndex() {
	this.closest('.nm-component').removeAttribute('tabindex');
}

/**
 * When the select2 dropdown is closing, restore the bootstrap component tabindex attribute.
 * @description select2-bootstrap component HACK
 */
function restoreTabIndex() {
	this.closest('.nm-component').setAttribute('tabindex', -1);
}

/**
 * When Select2 dropdown is open, the component cannot scroll
 * So close it when component scrolling event is detected.
 *
 * This hack works on the offcanvas element by default.
 * However it only works on the modal if the modal body is allowed to scroll
 * by adding .modal-dialog-scrollable to .modal-dialog.
 *
 * (This hack seems to be necessary only on the frontend.)
 *
 * @description select2-bootstrap component HACK
 */
function closeSelect2OnScroll() {
	var $component = $(this);
	if ($component.find('.select2-hidden-accessible').length) {
		var el = $component[0].querySelector('.offcanvas-body') || $component[0].querySelector('.modal-body');
		$(el).on('scroll', function () {
			$(el).find('.select2-hidden-accessible').each(function () {
				var $self = $(this);
				if ($self.select2('isOpen')) {
					$self.select2('close');
				}
			});
		});
	}
}



